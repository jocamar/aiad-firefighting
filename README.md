A simple Java application, written using Jadex, for simulating intelligent firefighter agents in forest fire situations.

Developed for the Distributed Artificial Intelligence Agents course at FEUP by João Marinheiro and Daniel Pereira.