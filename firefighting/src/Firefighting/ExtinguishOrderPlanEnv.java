package Firefighting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import Firefighting.World.Fire;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.PlanFailureException;
import jadex.commons.future.DefaultResultListener;
import jadex.commons.future.IFuture;

/**
 *  Send a unit to a specific location
 */
@Plan
public class ExtinguishOrderPlanEnv {
	
	@PlanCapability
	protected CommanderBDI agent;
	
	@PlanAPI
	protected IPlan rplan;
	
	protected int action = -1;
	
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		if(agent.isFinished())
			return;
		
		agent.updateQuads();
		final Quad worstQuad = agent.getWorstQuad();
		
		if(worstQuad!=null) {
			Fire target = worstQuad.findWorstFire();
			
			if(target!=null) {
				Squad bestSquad = agent.getBestSquad(target.getPosition(),"extinguish");
				final String msg = target.getPosition().getXAsInteger() + "-" + target.getPosition().getYAsInteger() + "-" + bestSquad.getId() + "-" + worstQuad.getId() + "-" + "extinguish";
				
				IFuture<Collection<IChatService>> chatservices = agent.getAgent().getRequiredServices("chatservices");
				chatservices.addResultListener(new DefaultResultListener<Collection<IChatService>>()
				{
					public void resultAvailable(Collection<IChatService> result)
					{
						for(Iterator<IChatService> it=result.iterator(); it.hasNext(); ) {
							IChatService cs = it.next();
							cs.message(agent.getAgent().getComponentIdentifier().getLocalName(), msg);
						}
					}
				});
			}
		}
		throw new PlanFailureException();
	}

	@SuppressWarnings("unused")
	private Fire findWorstFire() {
		ArrayList<Fire> fires = new ArrayList<Fire>();
		Fire worstFire = null;
		double worstInt = 0;
		for(int i=0; i<Fire.instances.size(); i++) {
			Fire f = Fire.instances.get(i);
			
			if(f.getIntensity()>=worstInt && !f.isTackled()) {
				worstInt = f.getIntensity()*CommanderBDI.dangerPerFire;
				fires.add(0,f);
			}
			else if(f.getIntensity()==worstInt && !f.isTackled()) {
				fires.add(0,f);
			}
		}
		
		if(fires.size()>0)
			worstFire = fires.get(0);
		
		return worstFire;
	}
	
	@PlanAborted
	public void	aborted()
	{
		
	}	
}
