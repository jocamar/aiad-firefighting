package Firefighting;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.SpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

import java.util.ArrayList;
import java.util.Collection;

import Firefighting.MovementCapability.GoToPos;

/**
 *  Go to a specified position.
 */
@Plan
public class AssureSafetyPlanEnv 
{
	//-------- attributes --------

	@PlanCapability
	protected BaseAgentBDI fighter;
	
	@PlanAPI
	protected IPlan rplan;
	
	protected int action = -1;
	
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		if(!fighter.isDead()) {
			fighter.setRunning(true);
			fighter.getFireTarget().setTackled(false);
			IVector2 curPos = fighter.getPosition();
			IVector2 newPos = getClosestSafePos(curPos);
			
			if(newPos != null) {
				GoToPos go = fighter.getMoveCapa().new GoToPos(newPos);
				rplan.dispatchSubgoal(go).get();
			}
			else System.out.println("Couldn't find a safe position.");
			
			fighter.setRunning(false);
		}
		
	}
	
	@SuppressWarnings("unchecked")
	private IVector2 getClosestSafePos(IVector2 curPos) {
		Grid2D env = (Grid2D)fighter.getEnvironment();
		IVector2 size = env.getAreaSize();
		
		int y = curPos.getYAsInteger() - 10;
		if(y < 0)
			y = 0;
		
		int x = curPos.getXAsInteger() - 10;
		if(x < 0)
			x = 0;
		
		IVector2 safePos = null;
		double curPriority = 0;
		for(int i = y; i < size.getYAsInteger() && i < curPos.getYAsInteger()+10; i++) {
			for(int j = x; j < size.getXAsInteger() && j < curPos.getXAsInteger()+10; j++) {
				IVector2 pos = new Vector2Int(j,i);
				
				String[] typesAgents = {"firefighter", "firetruck"};
				Collection<ISpaceObject> agents = env.getSpaceObjectsByGridPosition(pos,typesAgents);
				if(agents != null && agents.size() > 0)
					continue;
				
				//checks if there is an active fire near the position
				IVector2[] nearPos = Util.getNeighbors(env, pos, BaseAgentBDI.fireDangerRange);
				String[] types = {"fire"};
				ArrayList<SpaceObject> objects = new ArrayList<SpaceObject>();
				for(IVector2 position : nearPos) {
					objects.addAll((Collection<? extends SpaceObject>) env.getSpaceObjectsByGridPosition(position, types));
				}
				
				boolean safe = true;
				for(SpaceObject obj : objects) {
					if(!((Boolean) obj.getProperty("extinct")).booleanValue())
						safe = false;
				}
				
				//checks if there are any trucks near the position
				IVector2[] truckPos = Util.getNeighbors(env, pos, 3);
				String[] truck = {"truck"};
				ArrayList<SpaceObject> firetrucks = new ArrayList<SpaceObject>();
				for(IVector2 position : truckPos) {
					try {
					firetrucks.addAll((Collection<? extends SpaceObject>) env.getSpaceObjectsByGridPosition(position, truck));
					} catch(Exception e) {};
				}
				
				//calculates priority based on proximity and trucks
				double priority = -curPos.getDistance(pos).getAsDouble();
				
				for(@SuppressWarnings("unused") SpaceObject obj : firetrucks) {
					priority += 50;
				}
				
				if(safe) {
					if(safePos == null) {
						safePos = pos;
						curPriority = priority;
					}
					else if (priority > curPriority) {
						safePos = pos;
						curPriority = priority;
					}
				}	
			}
		}
		return safePos;
	}
	
	@PlanAborted
	public void	aborted()
	{
		
	}
}
