package Firefighting;

import gui.FirefightingGui;
import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Body;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalRecurCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Plans;
import jadex.bdiv3.annotation.Trigger;
import jadex.bridge.service.types.clock.IClockService;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

import java.util.ArrayList;
import java.util.List;

import Firefighting.World.Fire;
import Firefighting.World.Vegetation;
import Firefighting.World.Victim;
import Firefighting.World.Wind;

/**
 * 
 */
@Agent
@Plans(
{
	@Plan(trigger=@Trigger(goals=CommanderBDI.ExtinguishOrder.class), body=@Body(ExtinguishOrderPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=CommanderBDI.SaveVictimOrder.class), body=@Body(SaveVictimOrderPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=CommanderBDI.UpdateTeam.class), body=@Body(UpdateTeamPlanEnv.class))
})
@ProvidedServices(@ProvidedService(type=IChatService.class, implementation=@Implementation(ChatService.class)))
@RequiredServices(
{
	@RequiredService(name="clockservice", type=IClockService.class, binding=@Binding(scope=Binding.SCOPE_PLATFORM)),
	@RequiredService(name="chatservices", type=IChatService.class, multiple=true, binding=@Binding(dynamic=true, scope=Binding.SCOPE_PLATFORM))
})
public class CommanderBDI
{
	public static CommanderBDI instance = null;
	
	public static FirefightingGui gui = null;
	

	@Agent
	protected BDIAgent agent;
	
	/** The environment. */
	protected Grid2D env = (Grid2D)agent.getParentAccess().getExtension("myff2dspace").get();
	
	/** My information. */
	protected ISpaceObject myself = env.getAvatar(agent.getComponentDescription(), agent.getModel().getFullName());
	
	
	/** ------------------- Beliefs ---------------------------*/
	/** The fires. */
	@Belief
	protected List<Fire> fires = Fire.instances;
	
	@Belief
	protected List<FirefighterBDI> firefighters = Util.fighters;
	
	@Belief
	protected List<FiretruckBDI> firetrucks = Util.trucks;
	
	@Belief
	public static List<Quad> quads = new ArrayList<Quad>();
	
	@Belief
	protected List<Squad> squads = new ArrayList<Squad>();
	
	/** The victims. */
	@Belief
	protected List<Victim> victims = Victim.instances;
	
	/** The wind */
	@Belief
	protected Wind wind = Wind.instance;
	
	@Belief
	protected String direction = "None";
	
	@Belief(updaterate=500)
	protected boolean finished = checkFinished();
	
	@Belief(updaterate=500)
	protected boolean failed = false;
	
	@Belief(updaterate=500)
	protected long currenttime = System.currentTimeMillis();


	public static Double tilesToDanger;
	public static Double dangerPerFire;
	public static Double dangerPerVeg;
	public static Double businessInfluence;
	public static Double distanceInfluence;
	public static Double endangeredInfluence;
	public static Double squadDangerInfluence;
	
/** ------------------- Body -------------------------*/
	
	/**
	 *  The agent body.
	 */
	@AgentBody
	synchronized public void body()
	{
		tilesToDanger = Double.parseDouble(this.getAgent().getArgument("tilestodanger").toString());
		dangerPerFire = Double.parseDouble(this.getAgent().getArgument("dangerperfire").toString());
		dangerPerVeg = Double.parseDouble(this.getAgent().getArgument("dangerperveg").toString());
		businessInfluence = Double.parseDouble(this.getAgent().getArgument("businessinfluence").toString());
		distanceInfluence = Double.parseDouble(this.getAgent().getArgument("distanceinfluence").toString());
		endangeredInfluence = Double.parseDouble(this.getAgent().getArgument("endangeredinfluence").toString());
		squadDangerInfluence = Double.parseDouble(this.getAgent().getArgument("squaddangerinfluence").toString());
		agent.dispatchTopLevelGoal(new ExtinguishOrder());
		agent.dispatchTopLevelGoal(new SaveVictimOrder());
		agent.dispatchTopLevelGoal(new UpdateTeam());
		quads.clear();
		CommanderBDI.instance = this;
		gui = new FirefightingGui();
		
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                FirefightingGui.createAndShowGUI();
            }			
        });
		
		createQuads();
		try {wait(1000);}
		catch (InterruptedException e) {e.printStackTrace();}
		createSquads();
	}

	/** ------------------- Goals -------------------------*/
	/**
	 *  Goal for extinguishing fire
	 */
	@Goal(recur=true,retrydelay=5000)
	public class ExtinguishOrder
	{
		/**
		 * 
		 */
		@GoalRecurCondition(beliefs="currenttime")
		public boolean checkRecur() 
		{
			updateQuads();
			return true;
		}
	}
	
	/**
	 *  Goal for rescuing victims
	 */
	@Goal(recur=true)
	public class SaveVictimOrder
	{
		@GoalRecurCondition(beliefs="currenttime")
		public boolean checkRecur() 
		{
			updateQuads();
			checkVictimStatus();
			return true;
		}

		private boolean checkVictimStatus() {
			for(int i=0; i<victims.size();i++) {
				IVector2 vPos = victims.get(i).getPosition();
				
				if(victims.get(i).isDead() || victims.get(i).isSaved()) continue;
				
				if(calculateDanger(vPos)>0) {
					victims.get(i).setEndangered(true);
				}
				else
					victims.get(i).setEndangered(false);
			}
			return true;
		}
	}
	
	@Goal(recur=true,retrydelay=5000)
	public class UpdateTeam
	{
		/**
		 * 
		 */
		@GoalRecurCondition(beliefs="currenttime")
		public boolean checkRecur()
		{
			return true;
		}
	}
	
	/** ------------------- Getters -------------------------*/
	
	public Grid2D getEnvironment()
	{
		return env;
	}

	public IVector2 getPosition()
	{
		return (IVector2)myself.getProperty(Space2D.PROPERTY_POSITION);
	}
	
	public List<Victim> getVictims()
	{
		return victims;
	}
	
	public List<Fire> getFires()
	{
		return fires;
	}
	
	public List<FirefighterBDI> getFirefighters()
	{
		return firefighters;
	}
	
	public List<FiretruckBDI> getFiretruck()
	{
		return firetrucks;
	}
	
	public Wind getWind() {
		return wind;
	}

	public ISpaceObject getMyself()
	{
		return myself;
	}
	
	public BDIAgent getAgent()
	{
		return agent;
	}

	public List<Squad> getSquads() {
		return squads;
	}

	public void setSquads(List<Squad> squads) {
		this.squads = squads;
	}
	
	public static FirefighterBDI getClosestFirefighter(CommanderBDI agent, IVector2 target) {
		FirefighterBDI closest = null;
		for(int i = 0; i < agent.getFirefighters().size(); i++) {
			FirefighterBDI curr = agent.getFirefighters().get(i);
			IVector2 fighterPos = (IVector2)curr.getPosition();
			if(curr.isDead())
				continue;
			
			if(closest == null) {
				closest = curr;
			}
			else {
				IVector2 bestPos = (IVector2)closest.getPosition();
				if (fighterPos.getDistance(target).getAsDouble() < 
							bestPos.getDistance(target).getAsDouble()) {
					closest = curr;
				}
			}
		}
		
		return closest;
	}
	
	public void createQuads() {
		int quadrants = 4;
		IVector2 mapSize = this.getEnvironment().getAreaSize();
		double xPerQuad = Math.floor(mapSize.getXAsDouble()/quadrants);
		double yPerQuad = Math.floor(mapSize.getYAsDouble()/quadrants);
		double xRemainder = mapSize.getXAsDouble()%quadrants;
		double yRemainder = mapSize.getYAsDouble()%quadrants;
		
		for(int i=0;i<quadrants;i++) {
			int ySum=0;
			if(yRemainder>0) {
				ySum++;
				yRemainder--;
			}
			for(int k=0;k<quadrants;k++) {
				int xSum=0;
				if(xRemainder>0) {
					xSum++;
					xRemainder--;
				}
				
				CommanderBDI.quads.add(new Quad(xPerQuad*k,yPerQuad*i,xSum+xPerQuad,ySum+yPerQuad,0));
			}
		}
	}
	
	/**
	 * Updates all quadrants of the map. It creates a sum of the intensities of all fires in the quad,
	 * and if there are fires, adds also the sum of the density of all vegetation. For every squadron
	 * in the area, it substracts 25. In the end, danger is the average of all the above.
	 */
	synchronized public void updateQuads() {
		for(int i=0;i<quads.size();i++) {
			Quad quad = quads.get(i);
			double totalInt = 0;
			double noFires = 0;
			for(int j=0;j<fires.size();j++) {
				Fire fire = fires.get(j);
				if(fire.getPosition().getXAsDouble() >= quad.getStartX() &&
						fire.getPosition().getXAsDouble() <= (quad.getStartX()+quad.getLength()) &&
						fire.getPosition().getYAsDouble() >= quad.getStartY() &&
						fire.getPosition().getYAsDouble() <= (quad.getStartY()+quad.getHeight())) {
					totalInt+=fire.getIntensity()*dangerPerFire;
					noFires++;
				}
			}
			
			if(quad.hasSquadAssigned())
				totalInt-=25*quad.getSquadsAssigned()*squadDangerInfluence;
			
			if(noFires>0) {
				for(int j=0;j<Vegetation.instances.size();j++) {
					Vegetation veg = Vegetation.instances.get(j);
					if(veg.getPosition().getXAsDouble() >= quad.getStartX() &&
							veg.getPosition().getXAsDouble() <= (quad.getStartX()+quad.getLength()) &&
							veg.getPosition().getYAsDouble() >= quad.getStartY() &&
							veg.getPosition().getYAsDouble() <= (quad.getStartY()+quad.getHeight())) {
						totalInt+=veg.getDensity()*dangerPerVeg;
						noFires++;
					}
				}
			}
			
			double danger=0;
			if(noFires>0) {
				danger = totalInt/noFires;
				quad.setDanger(danger);
			}
		}
	}
	
	/**
	 * Returns the most dangerous quad, which is the one with the highest danger and below 4 squads assigned
	 * @return worstQuad most dangerous quad
	 */
	synchronized public Quad getWorstQuad() {
		double worst=0;
		Quad quad = null;
		for(int i=0;i<quads.size();i++) {
			if((worst<quads.get(i).getDanger()
					&& quads.get(i).getSquadsAssigned()<4) || quad==null) {
				worst=quads.get(i).getDanger() - 25*quads.get(i).getSquadsAssigned()*squadDangerInfluence;
				quad = quads.get(i);
			}
		}
		return quad;
	}
	
	/**
	 * Equally distributes all firefighters and trucks to squads.
	 */
	public void createSquads() {
		double firetrucks = Util.trucks.size();
		double firefighters = Util.fighters.size();

		ArrayList<BaseAgentBDI> members = new ArrayList<BaseAgentBDI>();
		double trucksPerSquad = 0;
		double fightersPerSquad = 0;
		double squads = 0;
		fightersPerSquad=(int) Math.ceil(firefighters/firetrucks);
		trucksPerSquad=(int) Math.ceil(firetrucks/firefighters);
		double trucksOut = 0;
		double fightersOut = 0;
		
		if(firetrucks > firefighters)
			fightersOut = firetrucks%firefighters;
		else
			trucksOut = firefighters%firetrucks;

		if(fightersPerSquad>trucksPerSquad) squads= Math.ceil(firetrucks/trucksPerSquad);
		squads= Math.ceil(firefighters/fightersPerSquad);
		
		
		int i=0;
		int k=0;
		for(int j=0;j<squads;j++) {
			for(i=0;i<trucksPerSquad&&(i+j*(int)trucksPerSquad)<this.firetrucks.size();i++) members.add(this.firetrucks.get(i+j*(int)trucksPerSquad));
			for(k=0;k<fightersPerSquad&&(k+j*(int)fightersPerSquad)<this.firefighters.size();k++) members.add(this.firefighters.get(k+j*(int)fightersPerSquad));
			
			if(fightersOut>0 && (k+j*(int)fightersPerSquad)<this.firefighters.size()) {
				members.add(this.firefighters.get(k+j*(int)fightersPerSquad));
				k++;
				fightersOut--;
			}
			if(trucksOut>0 && (i+j*(int)trucksPerSquad)<this.firetrucks.size()) {
				members.add(this.firetrucks.get(i+j*(int)trucksPerSquad));
				i++;
				trucksOut--;
			}
			this.squads.add(new Squad(members));
			members.clear();
		}
	}
	
	synchronized private int calculateDanger(IVector2 pos) {
		int fires = 0;
		int rtnVal = 0;
		int size = this.fires.size();
		if(pos != null) {
			for(int i = 0; i < size; i++) {
				Fire f = this.fires.get(i);
				if(f.getPosition().getDistance(pos).getAsDouble()<tilesToDanger) {
					fires++;
					rtnVal += f.getIntensity()*dangerPerFire;
				}
			}
		}
		if(fires==0)
			return rtnVal;
		else {
			rtnVal = rtnVal/fires;
		
			return rtnVal;
		}
	}
	
	synchronized public Victim getWorstVictimByQuad(Quad quad) {
		Victim bestVictim = null;
		ArrayList<Victim> victims = quad.getVictims();
		
		double bestPriority = 0;
		for(int i=0; i<victims.size(); i++) {
			double currPriority = 0;
			Victim currVictim = victims.get(i);
			currPriority += calculateDanger(currVictim.getPosition());
				
			if(currVictim.isEndangered())
				currPriority+= 50*endangeredInfluence;
			
			if(currVictim.isSaved())
				currPriority=0;
			
			if(bestVictim!=null) {
				if(bestPriority<currPriority) {
					bestVictim = currVictim;
					bestPriority = currPriority;
				}
			}
			else {
				bestVictim = currVictim;
				bestPriority = currPriority;
			}
		}
		
		if(bestVictim.isSaved() || !bestVictim.isEndangered() || bestVictim.isDead())
			return null;
		else
			return bestVictim;
	}
	
	synchronized public Squad getBestSquad(IVector2 firePos,String operation) {
		Squad best = null;
		double bestAverage = 0;
		
		for(int i=0; i<Squad.squads.size(); i++) {
			Squad currSquad = Squad.squads.get(i);
			double currAverage = 0;
			double size = currSquad.getMembers().size();
			double distSum = 0;
			for(int j=0; j<currSquad.getMembers().size(); j++) {
				BaseAgentBDI currMember = currSquad.getMembers().get(j);
				distSum += currMember.getPosition().getDistance(firePos).getAsDouble()*distanceInfluence;
				if(operation.equals("save")) {
					if(currMember.getVictimTarget()!=null) {
						distSum+=200*businessInfluence;
					}
				}
				else if(operation.equals("extinguish"))
					if(currMember.getFireTarget()!=null)
						distSum+=200*businessInfluence;
			}

			currAverage=distSum/size;
			
			if(currSquad.getAssignedQuad()!=null)
				currAverage-=currSquad.getAssignedQuad().getDanger();
			
			if(best==null) {
				bestAverage=currAverage;
				best = currSquad;
			} else {
				if(bestAverage<currAverage) {
					bestAverage=currAverage;
					best = currSquad;
				}
			}
		}
		return best;
	}

	synchronized public Quad getMostEndangeredQuad() {
		Quad bestQuad = null;
		double bestPriority = 0;
		for(int i=0;i<quads.size();i++) {
			double currPriority = 0;
			Quad currQuad = quads.get(i);
			for(int j=0;j<currQuad.getVictims().size();j++) {
				Victim currVictim = currQuad.getVictims().get(j);
				
				if(currVictim.isDead())
					continue;
				
				currPriority += calculateDanger(currVictim.getPosition());
				
				if(currVictim.isEndangered())
					currPriority+= 50;
			}
			
			currPriority += currQuad.getDanger();
			
			if(currQuad.hasSquadAssigned())
				currPriority-= 50*currQuad.getSquadsAssigned();
			
			if(bestQuad!=null) {
				if(bestPriority<currPriority) {
					bestQuad = currQuad;
					bestPriority = currPriority;
				}
			}
			else {
				bestQuad = currQuad;
				bestPriority = currPriority;
			}
		}
		
		return bestQuad;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}



	public boolean isFinished() {
		return finished;
	}



	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	
	public void removeSquad(Squad s) {
		s.getAssignedQuad().setDanger(s.getAssignedQuad().getDanger()+25);
		s.getAssignedQuad().setSquadsAssigned(s.getAssignedQuad().getSquadsAssigned()-1);
		this.squads.remove(s);
	}
	
	
	
	private boolean checkFinished() {
		if(!this.finished) {
			boolean finished = true;
			for(int k=0; k<this.getFires().size(); k++) {
				if(!this.getFires().get(k).isExtinct()) {
					finished = false;
					break;
				}
			}
	
			if(finished) {
				System.out.println("All fires have been put out; good job, team.");
				System.out.println("Aftermath:");
				System.out.println("Lost vegetation: " + Vegetation.getBurnt() + "/" + Vegetation.instances.size());
				System.out.println("Fires put out: " + Fire.getExtinct());
				System.out.println("Fighters lost: " + FirefighterBDI.getDead()+"/" + Util.fighters.size());
				System.out.println("Trucks lost: " + FiretruckBDI.getDead()+"/" + Util.trucks.size());
				System.out.println("Victims saved: " + Victim.getSaved()+"/" + Victim.instances.size());
				System.out.println("Victims dead: " + Victim.getDead()+"/" + Victim.instances.size());
			}
			
			return finished;
		}
		
		return true;
	}

}
