package Firefighting;

import java.util.Set;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.extension.envsupport.environment.SpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.math.IVector2;
import Firefighting.World.Fire;

/**
 *  The go action for moving one field in one of four directions.
 */
@Plan
public class ShootWaterPlanEnv
{
	public int waterEffect = 7;
	
	@PlanCapability
	protected FirefighterBDI fighter;
	
	@PlanAPI
	protected IPlan rplan;
	//-------- methods --------
	
	/**
	 * Performs the action.
	 * @param parameters parameters for the action
	 * @param space the environment space
	 * @return action return value
	 */
	@PlanBody
	public void body()
	{
		if(!fighter.isDead()) {
			Fire target = fighter.getFireTarget();
			
			if(target != null) {
				if(!fighter.checkTruckProximity()) {
					fighter.setSupplied(false);
					return;
				}
				
				fighter.setSupplied(true);
				
				if(!target.isExtinct() && !fighter.running && target.getPosition().getDistance(fighter.getPosition()).getAsDouble() < 4) {
					target.setTackled(true);
					target.setIntensity((int) (target.getIntensity()-waterEffect*FirefighterBDI.waterEffectiveness));
					if(target.getIntensity()<=0) {
						target.setExtinguished(true);
						target.setTackled(false);
						target.setIntensity(0);
						fighter.setFireTarget(null);
					}
				}
				else {
					target.setTackled(false);
					fighter.setFireTarget(null);
				}
			}
		}
	}
	
	public boolean checkTruckProximity(FirefighterBDI fighter) {
		IVector2 pos = fighter.getPosition();
		String[] types = {"firetruck"};
		Grid2D grid = (Grid2D)fighter.getEnvironment();
		Set<SpaceObject> objects = grid.getNearGridObjects(pos, 3, types);
		if(objects.size()>0)
			return true;
		
		return false;
	}
}
