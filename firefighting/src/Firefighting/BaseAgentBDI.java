package Firefighting;

import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Deliberation;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Goal.ExcludeMode;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;
import jadex.micro.annotation.Agent;

import java.util.ArrayList;
import java.util.List;

import Firefighting.World.Fire;
import Firefighting.World.Victim;
import Firefighting.World.Wind;

//import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * 
 */
@Agent
public abstract class BaseAgentBDI
{
	@Agent
	protected BDIAgent agent;
	
	@Capability
	protected MovementCapability movecapa = new MovementCapability();
	
	/** The environment. */
	protected Grid2D env = (Grid2D)agent.getParentAccess().getExtension("myff2dspace").get();
	
	/** My information. */
	protected ISpaceObject myself = env.getAvatar(agent.getComponentDescription(), agent.getModel().getFullName());
	
	
	/** ------------------- Beliefs ---------------------------*/
	/** The fires. */
	@Belief
	protected List<Fire> fires = new ArrayList<Fire>();
	
	/** The firefighters. */
	@Belief
	protected List<FirefighterBDI> firefighters = new ArrayList<FirefighterBDI>();
	
	/** The firetrucks. */
	@Belief
	protected List<FiretruckBDI> firetrucks = new ArrayList<FiretruckBDI>();
	
	/** The victims. */
	@Belief
	protected List<Victim> victims = new ArrayList<Victim>();
	
	/** The wind */
	@Belief
	protected Wind wind = null;
	
	/** Danger level */
	@Belief(updaterate=1000)
	protected int danger = calculateDanger();
	
	/** Health */
	@Belief
	protected int health = 100;
	
	@Belief(updaterate=1000)
	protected boolean dead = updateSelf();
	
	@Belief(updaterate=1000)
	protected long currenttime = System.currentTimeMillis();
	
	@Belief
	protected boolean running = false;
	
	@Belief
	protected Fire fireTarget = null;
	
	@Belief
	protected Victim victimTarget = null;
	
	@Belief
	protected Squad squad = null;

	public static Double dmgPerFlame;

	public static int fieldOfView;

	public static int fireDangerRange;
	
	/** ------------------- Goals -------------------------*/
	
	@Goal(excludemode=ExcludeMode.Never, retrydelay=100, deliberation=@Deliberation(inhibits={ExtinguishFirePlanEnv.class,SupplyWaterPlanEnv.class}, cardinalityone=true))
	public class AssureSafety
	{	
		@GoalMaintainCondition(beliefs="danger")
		protected boolean maintain() {
			return danger<=1000;
		}
 
		@GoalTargetCondition(beliefs="danger")
		protected boolean target() {
			return danger <= 10;
		}
	}
	
	/** ------------------- Getters -------------------------*/
	
	public Grid2D getEnvironment()
	{
		return env;
	}

	private boolean updateSelf() {
		if(dead)
			return true;
		
		checkSurroundings();
		return checkHealth();
	}

	/**
	 * @return
	 */
	private boolean checkHealth() {
		if(this.getPosition()==null)
			return false;
		
		Fire fire = Fire.getFireAt(this.getPosition());
		if(fire!=null && !fire.isExtinct()) {
			health-=dmgPerFlame;
		}
		
		if(health<=0) {
			System.out.println("Agent dead!");
			this.setDead(true);
			this.setHealth(0);
			return true;
		}
		else {
			this.setHealth(health);
			return false;
		}
	}

	/**
	 * 
	 */
	private void checkSurroundings() {
		if(this.getPosition()==null)
			return;
		
		IVector2 pos = this.getPosition();
		int xI = pos.getXAsInteger()-fieldOfView;
		if(xI<0)
			xI=0;
		int yI = pos.getYAsInteger()-fieldOfView;
		if(yI<0)
			yI=0;
		int xF = pos.getXAsInteger()+fieldOfView;
		if(xF>(this.getEnvironment().getAreaSize().getXAsInteger()-1))
				xF=this.getEnvironment().getAreaSize().getXAsInteger()-1;
		int yF = pos.getYAsInteger()+fieldOfView;
		if(yF>(this.getEnvironment().getAreaSize().getYAsInteger()-1))
			yF=this.getEnvironment().getAreaSize().getYAsInteger()-1;
		for(int y=yI;y<yF;y++) {
			for(int x=xI;x<xF;x++) {
				Fire fire = Fire.getFireAt(new Vector2Int(x,y));
				if(fire!=null) {
					if(!fire.isExtinct() && !this.knowsFire(fire)) {
						if(fires!=null) {
							this.fires.add(fire);
						}
					}
				}
				
				Victim victim = Victim.getVictimAt(new Vector2Int(x,y));
				if(victim!=null && !this.victims.contains(victim)) {
					if(victims!=null) {
						this.victims.add(victim);
					}
				}
			}
		}
	}
	
	private boolean knowsFire(Fire fire) {
		if(fires!=null) {
			for(int i=0;i<fires.size();i++) {
				if(Integer.valueOf(fires.get(i).getSelf().getId().toString()).equals(
						Integer.valueOf(fire.getSelf().getId().toString()))) {
					return true;
				}
			}
		}
		return false;
	}

	private int calculateDanger() {
		int rtnVal = 0;
		if(fires!=null) {
			int size = fires.size();
			if(getPosition() != null) {
				for(int i = 0; i < size; i++) {
					Fire f = fires.get(i);
					if(f.getPosition().getDistance(getPosition()).getAsDouble()<3) {
						rtnVal += f.getIntensity()*CommanderBDI.dangerPerFire;
					}
				}
			}
		}
		return rtnVal;
	}

	public IVector2 getPosition()
	{
		return (IVector2)myself.getProperty(Space2D.PROPERTY_POSITION);
	}
	
	public List<Victim> getVictims()
	{
		return victims;
	}
	
	protected boolean hasVictims()
	{
		return victims.size()>0;
	}
	
	public List<Fire> getFires()
	{
		return fires;
	}

	protected boolean hasFires()
	{
		return fires.size()>0;
	}
	
	public List<FirefighterBDI> getFirefighters()
	{
		return firefighters;
	}
	
	public List<FiretruckBDI> getFiretruck()
	{
		return firetrucks;
	}
	
	public Wind getWind() {
		return wind;
	}

	public ISpaceObject getMyself()
	{
		return myself;
	}
	
	public BDIAgent getAgent()
	{
		return agent;
	}
	
	public int getDanger() {
		return danger;
	}
	
	/**
	 *  Get the movecapa.
	 *  @return The movecapa.
	 */
	public MovementCapability getMoveCapa()
	{
		return movecapa;
	}
	
	public boolean isRunning() {
		return running;
	}
	
	public void setRunning(boolean val) {
		running = val;
	}
	
	public Fire getFireTarget() {
		return fireTarget;
	}
	
	public Victim getVictimTarget() {
		return victimTarget;
	}

	synchronized public void setFireTarget(Fire target) {
		if(this.fireTarget != null)
			this.fireTarget.targetNum--;
		this.fireTarget = target;
		if(target != null) {
			Integer id = Integer.valueOf(target.getSelf().getId().toString());
			myself.setProperty("target", id);
		}
		this.fireTarget.targetNum++;
	}
	
	synchronized public void setVictimTarget(Victim target) {
		this.victimTarget = target;
		if(target != null) {
			Integer id = Integer.valueOf(target.getSelf().getId().toString());
			myself.setProperty("victimtarget", id);
		}
	}
	
	synchronized public void setHoldPosition(boolean val) {
		myself.setProperty("holdPosition", val);
	}
	
	public void setDead(boolean val) {
		this.dead = val;
		myself.setProperty("dead", val);
	}
	
	public boolean isDead() {
		return ((Boolean) myself.getProperty("dead")).booleanValue();
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
		myself.setProperty("health", health);
	}

	public Squad getSquad() {
		return squad;
	}

	public void setSquad(Squad squad) {
		this.squad = squad;
		myself.setProperty("squad", squad.getId());
	}
}
