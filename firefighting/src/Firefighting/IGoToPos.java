package Firefighting;

import jadex.extension.envsupport.math.IVector2;

public interface IGoToPos {
	public IVector2 getPosition();
}
