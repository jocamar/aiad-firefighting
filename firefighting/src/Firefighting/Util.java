package Firefighting;

import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

import java.util.ArrayList;

public class Util {
	public static ArrayList<FirefighterBDI> fighters = new ArrayList<FirefighterBDI>();
	public static ArrayList<FiretruckBDI> trucks = new ArrayList<FiretruckBDI>();
	
	public static IVector2[] getNeighbors(Grid2D env, IVector2 position, int range) {
		IVector2 size = null;
		if(env != null)
			size = env.getAreaSize();
		else return null;
		
		if(position != null && size != null) {
	        int y = position.getYAsInteger() - range;
			if(y < 0)
				y = 0;
			
			int x = position.getXAsInteger() - range;
			if(x < 0)
				x = 0;
	        
			ArrayList<IVector2> positions = new ArrayList<IVector2>();
			for(int i = y; i < size.getYAsInteger() && i < position.getYAsInteger()+range; i++) {
				for(int j = x; j < size.getXAsInteger() && j < position.getXAsInteger()+range; j++) {
					positions.add(new Vector2Int(j,i));
				}
			}
			
			return positions.toArray(new IVector2[positions.size()]);
		} else return null;
    }
}
