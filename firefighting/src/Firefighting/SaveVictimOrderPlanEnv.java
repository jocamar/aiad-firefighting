package Firefighting;

import java.util.Collection;
import java.util.Iterator;

import Firefighting.World.Victim;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.PlanFailureException;
import jadex.commons.future.DefaultResultListener;
import jadex.commons.future.IFuture;

/**
 *  Send a unit to a specific location
 */
@Plan
public class SaveVictimOrderPlanEnv {
	
	@PlanCapability
	protected CommanderBDI agent;
	
	@PlanAPI
	protected IPlan rplan;
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		if(agent.isFinished())
			return;
		
		agent.updateQuads();
		final Quad worstQuad = agent.getMostEndangeredQuad();

		if(worstQuad!=null) {
			Victim target = agent.getWorstVictimByQuad(worstQuad);
			
			if(target!=null) {
				Squad bestSquad = agent.getBestSquad(target.getPosition(),"save");
				
				final String msg = target.getPosition().getXAsInteger() + "-" + target.getPosition().getYAsInteger() + "-" + bestSquad.getId() + "-" + worstQuad.getId() + "-" + "save";
				
				IFuture<Collection<IChatService>> chatservices = agent.getAgent().getRequiredServices("chatservices");
				chatservices.addResultListener(new DefaultResultListener<Collection<IChatService>>()
				{
					public void resultAvailable(Collection<IChatService> result)
					{
						for(Iterator<IChatService> it=result.iterator(); it.hasNext(); ) {
							IChatService cs = it.next();
							cs.message(agent.getAgent().getComponentIdentifier().getLocalName(), msg);
						}
					}
				});
			}
		}
		else {
			System.out.println("Could not find a quad.");
		}
		throw new PlanFailureException();
	}
	
	
	
	@PlanAborted
	public void	aborted()
	{
		
	}
}
