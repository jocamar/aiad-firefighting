package Firefighting;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.annotation.PlanReason;
import jadex.bdiv3.runtime.IPlan;
import jadex.commons.future.DelegationResultListener;
import jadex.commons.future.Future;
import jadex.extension.envsupport.environment.ISpaceAction;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import Firefighting.World.Fire;

/**
 *  Go to a specified position.
 */
@Plan
public class GoPlanEnv 
{
	//-------- attributes --------

	@PlanCapability
	protected IEnvAccess capa;
	
	@PlanAPI
	protected IPlan rplan;
	
	@PlanReason
	protected IGoToPos goal;
	
	int action = -1;
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		Grid2D env = (Grid2D) capa.getEnvironment();
		IVector2 target = goal.getPosition();
		ISpaceObject myself = capa.getMyself();
		
		ArrayList<IVector2> path = Astar(target);
		
		for(int i = 0; i < path.size(); i++)
		{
			IVector2 curr = path.get(i);
			String dir = null;
			IVector2 mypos = (IVector2)myself.getProperty(Space2D.PROPERTY_POSITION);
			
			if(curr.getXAsInteger() > mypos.getXAsInteger())
			{
				dir = GoAction.RIGHT;
			}
			else if(curr.getXAsInteger() < mypos.getXAsInteger())
			{
				dir = GoAction.LEFT;
			}
			else
			{
				if(curr.getYAsInteger() < mypos.getYAsInteger())
				{
					dir = GoAction.UP;
				}
				else if(curr.getYAsInteger() > mypos.getYAsInteger())
				{
					dir = GoAction.DOWN;
				}
				else {
					continue;
				};
			}
			
			Future<Void> fut = new Future<Void>();
			DelegationResultListener<Void> lis = new DelegationResultListener<Void>(fut, true);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(GoAction.DIRECTION, dir);
			params.put(ISpaceAction.OBJECT_ID, env.getAvatar(capa.getAgent().getComponentDescription()).getId());
			action	= env.performSpaceAction("go", params, lis); 
			fut.get();
		}
	}
	
	private class PNode {
		public IVector2 pos = null;
		public int g_cost = Integer.MAX_VALUE;
		public int f_cost = 0;
		public PNode cameFrom = null;
		
		public PNode(IVector2 pos) {
			this.pos = pos;
			this.g_cost = Integer.MAX_VALUE;
			this.f_cost = 0;
			this.cameFrom = null;
		}
		
		public ArrayList<IVector2> getPath() {
			ArrayList<IVector2> path = new ArrayList<IVector2>();
			PNode curr = this;
			path.add(curr.pos);
			while(curr.cameFrom != null) {
				curr = curr.cameFrom;
				path.add(curr.pos);
			}
			Collections.reverse(path);
			return path;
		}
		
		public int calculateF(IVector2 target) {
			int f = g_cost + pos.getDistance(target).getAsInteger();
			
			IVector2 fpos = new Vector2Int(pos.getXAsInteger()-1,pos.getYAsInteger());
			Fire fire = Fire.getFireAt(fpos);
			if(fire != null)
				f += fire.getIntensity()*CommanderBDI.dangerPerFire;
			
			fpos = new Vector2Int(pos.getXAsInteger()+1,pos.getYAsInteger());
			fire = Fire.getFireAt(fpos);
			if(fire != null)
				f += fire.getIntensity()*CommanderBDI.dangerPerFire;
			
			fpos = new Vector2Int(pos.getXAsInteger(),pos.getYAsInteger()+1);
			fire = Fire.getFireAt(fpos);
			if(fire != null)
				f += fire.getIntensity()*CommanderBDI.dangerPerFire;
			
			fpos = new Vector2Int(pos.getXAsInteger(),pos.getYAsInteger()-1);
			fire = Fire.getFireAt(fpos);
			if(fire != null)
				f += fire.getIntensity()*CommanderBDI.dangerPerFire;
			
			fpos = new Vector2Int(pos.getXAsInteger(),pos.getYAsInteger());
			fire = Fire.getFireAt(fpos);
			if(fire != null)
				f += fire.getIntensity()*2*CommanderBDI.dangerPerFire;
			
			return f;
		}
	}
	
	private ArrayList<IVector2> Astar(IVector2 target) {
		ISpaceObject myself = capa.getMyself();
		IVector2 myPos = (IVector2) myself.getProperty(Space2D.PROPERTY_POSITION);
		
		ArrayList<ArrayList<PNode>> map = new ArrayList<ArrayList<PNode>>();
		for(int i = 0; i < capa.getEnvironment().getAreaSize().getXAsInteger(); i++) {
			map.add(new ArrayList<PNode>());
			for(int j = 0; j < capa.getEnvironment().getAreaSize().getYAsInteger(); j++) {
				PNode node = new PNode(new Vector2Int(i,j));
				map.get(i).add(node);
			}
		}
		
		ArrayList<PNode> closed = new ArrayList<PNode>();
		ArrayList<PNode> open = new ArrayList<PNode>();
		open.add(map.get(myPos.getXAsInteger()).get(myPos.getYAsInteger()));
		
		open.get(0).g_cost = 0;
		open.get(0).f_cost = open.get(0).calculateF(target);
		
		while(!open.isEmpty()) {
			PNode current = null;
			for(int i = 0; i < open.size(); i++) {
				if(current == null)
					current = open.get(i);
				else if(current.f_cost > open.get(i).f_cost) {
					current = open.get(i);
				}
			}
			
			if(current.pos.equals(target))
				return current.getPath();
			
			open.remove(current);
			closed.add(current);
			
			ArrayList<PNode> neighbors = new ArrayList<PNode>();
			try {
				neighbors.add(map.get(current.pos.getXAsInteger()-1).get(current.pos.getYAsInteger()));
			} catch (Exception e) {
				
			}
			
			try {
				neighbors.add(map.get(current.pos.getXAsInteger()+1).get(current.pos.getYAsInteger()));
			} catch (Exception e) {
				
			}
			
			try {
				neighbors.add(map.get(current.pos.getXAsInteger()).get(current.pos.getYAsInteger()-1));
			} catch (Exception e) {
				
			}
			
			try {
				neighbors.add(map.get(current.pos.getXAsInteger()).get(current.pos.getYAsInteger()+1));
			} catch (Exception e) {
				
			}
			
			for(int i = 0; i < neighbors.size(); i++) {
				PNode neighbor = neighbors.get(i);
				
				if(closed.contains(neighbor))
					continue;
				
				int tentative_g_cost = current.g_cost + current.pos.getDistance(neighbor.pos).getAsInteger();
				
				if(!open.contains(neighbor) || tentative_g_cost < neighbor.g_cost) {
					neighbor.cameFrom = current;
					neighbor.g_cost = tentative_g_cost;
					neighbor.f_cost = neighbor.calculateF(target);
					if(!open.contains(neighbor))
						open.add(neighbor);
				}
			}
		}
		
		return null;
	}
	
	@PlanAborted
	public void	aborted()
	{
		if(action!=-1)
		{
			capa.getEnvironment().cancelSpaceAction(action);
		}
	}
}

