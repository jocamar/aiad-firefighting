package Firefighting;

import java.util.Collection;
import java.util.Set;

import Firefighting.MovementCapability.GoToPos;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.annotation.PlanReason;
import jadex.bdiv3.runtime.IPlan;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.SpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

/**
 *  Go to a specified position.
 */
@Plan
public class FollowCommandPlanEnv 
{
	//-------- attributes --------

	@PlanCapability
	protected BaseAgentBDI agent;
	
	@PlanAPI
	protected IPlan rplan;
	
	@PlanReason
	protected FollowCommand goal;
	
	protected int action = -1;
	
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		if(!agent.isDead()) {
			IVector2 dest = goal.getDest();
			if(dest != null) {
				IVector2 newPos = getClosestSafePos(dest);
				if(newPos != null) {
					if(agent.getFireTarget() != null) {
						agent.getFireTarget().setTackled(false);
						agent.setFireTarget(null);
					}
					GoToPos go = agent.getMoveCapa().new GoToPos(newPos);
					rplan.dispatchSubgoal(go).get();
				}
			}
		}
	}
	

	private IVector2 getClosestSafePos(IVector2 firePos) {
		Grid2D env = (Grid2D)agent.getEnvironment();
		IVector2 size = env.getAreaSize();
		
		int y = firePos.getYAsInteger() - 4;
		if(y < 0)
			y = 0;
		
		int x = firePos.getXAsInteger() - 4;
		if(x < 0)
			x = 0;
		
		IVector2 safePos = null;
		for(int i = y; i < size.getYAsInteger() && i < firePos.getYAsInteger()+4; i++) {
			for(int j = x; j < size.getXAsInteger() && j < firePos.getXAsInteger()+4; j++) {
				IVector2 pos = new Vector2Int(j,i);
				String[] types = {"fire"};
				Set<SpaceObject> objects = env.getNearGridObjects(pos, 2, types);
				
				String[] typesAgents = {"firefighter", "firetruck"};
				
				Collection<ISpaceObject> agents = env.getSpaceObjectsByGridPosition(pos,typesAgents);
				if(agents != null && agents.size() > 0)
					continue;
				
				boolean safe = true;
				for(SpaceObject obj : objects) {
					if(!((Boolean) obj.getProperty("extinct")).booleanValue())
						safe = false;
				}
				
				if(safe) {
					if(safePos == null)
						safePos = pos;
					else if (firePos.getDistance(pos).getAsDouble() < firePos.getDistance(safePos).getAsDouble())
						safePos = pos;
				}
			}
		}
		return safePos;
	}

	@PlanAborted
	public void	aborted() {
		
	}
}

