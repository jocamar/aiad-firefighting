package Firefighting;

import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

import java.util.ArrayList;

public class Squad {
	public static int idcount = 0;
	public static ArrayList<Squad> squads = new ArrayList<Squad>();
	private ArrayList<BaseAgentBDI> members = new ArrayList<BaseAgentBDI>();
	private Quad assignedQuad = null;
	private int id;
	
	public Squad(ArrayList<BaseAgentBDI> members) {
		id=idcount;
		idcount++;
		
		this.members.addAll(members);
		for(int i=0;i<this.members.size();i++) {
			members.get(i).setSquad(this);
		}
		System.out.println("Created a squad " + id + " with " + members.size() + " members.");
		Squad.squads.add(this);
	}
	
	synchronized public ArrayList<IVector2> getBestSupplyPosition() {
		BaseAgentBDI mostDistantAgentX = null;
		BaseAgentBDI mostDistantAgentY = null;
		BaseAgentBDI closestAgentX = null;
		BaseAgentBDI closestAgentY = null;
		int mostDistantX = 0;
		int mostDistantY = 0;
		int closestX = 0;
		int closestY = 0;
		for(int i=0;i<members.size();i++) {
			if(members.get(i).getAgent().getComponentIdentifier().getLocalName().contains("Firetruck"))
				continue;
			
			if(mostDistantAgentX == null || mostDistantX>members.get(i).getPosition().getXAsInteger()) {
				mostDistantAgentX = members.get(i);
				mostDistantX = mostDistantAgentX.getPosition().getXAsInteger();
			}
			
			if(mostDistantAgentY == null || mostDistantY>members.get(i).getPosition().getYAsInteger()) {
				mostDistantAgentY = members.get(i);
				mostDistantY = mostDistantAgentY.getPosition().getYAsInteger();
			}
			
			if(closestAgentX == null || closestX<members.get(i).getPosition().getXAsInteger()) {
				closestAgentX = members.get(i);
				closestX = closestAgentX.getPosition().getXAsInteger();
			}
			
			if(closestAgentY == null || closestY<members.get(i).getPosition().getYAsInteger()) {
				closestAgentY = members.get(i);
				closestY = closestAgentY.getPosition().getYAsInteger();
			}	
		}
		
		mostDistantX -= FirefighterBDI.supplyRange;
		mostDistantY -= FirefighterBDI.supplyRange;
		
		if(mostDistantX <0)
			mostDistantX=0;
		if(mostDistantY <0)
			mostDistantY=0;
		
		closestX += FirefighterBDI.supplyRange;
		closestY += FirefighterBDI.supplyRange;
		
		if(closestX >=closestAgentX.getEnvironment().getAreaSize().getXAsInteger())
			mostDistantX=closestAgentX.getEnvironment().getAreaSize().getXAsInteger()-1;
		if(closestY >=closestAgentY.getEnvironment().getAreaSize().getYAsInteger())
			mostDistantY=closestAgentY.getEnvironment().getAreaSize().getYAsInteger()-1;
		
		ArrayList<IVector2> bestPositions = new ArrayList<IVector2>();
		int mostFighters = 0;
		for(int y=mostDistantY;y<closestY;y++) {
			for(int x=mostDistantX;x<closestX;x++) {
				IVector2 currPosition = new Vector2Int(x,y);
				int fighters=0;
				int startX = x-FirefighterBDI.supplyRange;
				int length = FirefighterBDI.supplyRange;
				int startY = y-FirefighterBDI.supplyRange;
				int height = FirefighterBDI.supplyRange;
				for(int i=0;i<members.size();i++) {
					BaseAgentBDI agent = members.get(i);
					if(agent.getPosition().getXAsDouble() >= startX &&
							agent.getPosition().getXAsDouble() <= (startX+length) &&
							agent.getPosition().getYAsDouble() >= startY &&
							agent.getPosition().getYAsDouble() <= (startY+height)) {
						fighters++;
					}
				}
				
				if(bestPositions.size()==0) {
					mostFighters=fighters;
					bestPositions.add(currPosition);
				}
				else {
					if(mostFighters<=fighters) {
						mostFighters=fighters;
						bestPositions.add(0,currPosition);
					}
				}
			}
		}
		
		return bestPositions;
	}

	public ArrayList<BaseAgentBDI> getMembers() {
		return members;
	}

	public void setMembers(ArrayList<BaseAgentBDI> members) {
		this.members = members;
	}
	
	public int getSquadCount() {
		return members.size();
	}
	
	synchronized public boolean removeMember(BaseAgentBDI agent) {
		return members.remove(agent);
	}

	public Quad getAssignedQuad() {
		return assignedQuad;
	}

	public void setAssignedQuad(Quad assignedQuad) {
		if(this.assignedQuad!=null)
			this.assignedQuad.setSquadsAssigned(this.assignedQuad.getSquadsAssigned()-1);
		this.assignedQuad = assignedQuad;
		this.assignedQuad.setSquadsAssigned(this.assignedQuad.getSquadsAssigned()+1);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
