package Firefighting;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import Firefighting.FirefighterBDI.Rescue;
import Firefighting.MovementCapability.GoToPos;
import Firefighting.World.Victim;

@Plan
public class RescuePlanEnv {
	@PlanCapability
	protected FirefighterBDI fighter;
	
	@PlanAPI
	protected IPlan rplan;
	//-------- methods --------
	
	/**
	 * Performs the action.
	 * @param parameters parameters for the action
	 * @param space the environment space
	 * @return action return value
	 */
	@PlanBody
	public void body()
	{
		Victim target = fighter.getVictimTarget();
		if(target != null) {
			if(!target.getPosition().equals(fighter.getPosition())) {
				GoToPos go = fighter.getMoveCapa().new GoToPos(target.getPosition());
				rplan.dispatchSubgoal(go).get();
				Rescue rescue = fighter.new Rescue();
				rplan.dispatchSubgoal(rescue).get();
				return;
			}
			
			if(target.isDead()) {
				fighter.setVictimTarget(null);
				return;
			}
			
			target.setSaved(true);
			
			if(target.isSaved()) {
				fighter.setVictimTarget(null);
				return;
			}
		}
	}
}
