package Firefighting.World;

import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Vegetation {
	public static ArrayList<Vegetation> instances = new ArrayList<Vegetation>();
	private static Grid2D environment = null;
	protected ISpaceObject myself;
	
	public Vegetation(int x, int y,  int density, Grid2D env) {
		Map<Object, Object> init = new HashMap<>();
		init.put("position", new Vector2Int(x,y));
		init.put("density", density);
		
		myself = env.createSpaceObject("vegetation", init, null);
		
		if(environment == null)
			environment = env;
		else if (environment != env) {
			environment = env;
			instances.clear();
		}
		
		instances.add(this);
	}
	
	public IVector2 getPosition()
	{
		return (IVector2)myself.getProperty(Space2D.PROPERTY_POSITION);
	}
	
	public int getDensity()
	{
		return ((Integer)myself.getProperty("density")).intValue();
	}
	
	public void setDensity(int newDensity) {
		myself.setProperty("density", newDensity);
	}
	
	public static Vegetation[] getAllAtPosition(IVector2 pos) {
		ArrayList<Vegetation> veg = new ArrayList<Vegetation>();
		for(Vegetation v : instances) {
			if(v.getPosition().equals(pos))
				veg.add(v);
		}
		
		return veg.toArray(new Vegetation[veg.size()]);
	}
	
	public static Vegetation getVegetationAt(IVector2 pos) {
		for(Vegetation v : instances){
			if(v.getPosition().equals(pos)) {
				return v;
			}
		}
		
		return null;
	}

	public static int getBurnt() {
		int amount = 0;
		for(int i=0;i<Vegetation.instances.size();i++) {
			if(Vegetation.instances.get(i).getDensity()<=0)
				amount++;
		}
		return amount;
	}
}
