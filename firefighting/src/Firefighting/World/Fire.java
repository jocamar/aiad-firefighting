package Firefighting.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector1Int;
import jadex.extension.envsupport.math.Vector2Int;

public class Fire {
	public static ArrayList<Fire> instances = new ArrayList<Fire>();
	protected static Grid2D environment = null;
	protected ISpaceObject myself;
	public int targetNum = 0;
	private boolean deleted = false;
	
	public Fire(int x, int y, int intensity, Grid2D env) {
		Map<Object, Object> init = new HashMap<>();
		init.put("position", new Vector2Int(x, y));
		init.put("intensity", intensity);
		init.put("tackled", false);
		init.put("extinct", false);

		myself = env.createSpaceObject("fire", init, null);
		if (environment == null)
			environment = env;
		else if (environment != env) {
			environment = env;
			instances.clear();
		}

		instances.add(this);
	}

	public IVector2 getPosition() {
		return (IVector2) myself.getProperty(Space2D.PROPERTY_POSITION);
	}

	public int getIntensity() {
		return ((Integer) myself.getProperty("intensity")).intValue();
	}

	public boolean isExtinct() {
		return ((Boolean) myself.getProperty("extinct")).booleanValue();
	}
	
	public boolean isTackled() {
		return ((Boolean) myself.getProperty("tackled")).booleanValue();
	}

	public void setIntensity(int newIntensity) {
		myself.setProperty("intensity", newIntensity);
	}

	public void setPosition(int x, int y) {
		myself.setProperty(Space2D.PROPERTY_POSITION, new Vector2Int(x, y));
	}

	public void setExtinguished(boolean newVal) {
		myself.setProperty("extinct", newVal);
	}
	
	public void setTackled(boolean newVal) {
		myself.setProperty("tackled", newVal);
	}

	public Vegetation[] getNearbyVegetation(int range, Grid2D environment) {
			List<ISpaceObject> nearbyObjects = new ArrayList<ISpaceObject>(environment.getNearObjects(getPosition(), new Vector1Int(range)));
			ArrayList<Vegetation> returnVal = new ArrayList<Vegetation>();
			
			if(nearbyObjects.size()>0) {
				for (int j = 0; j < nearbyObjects.size(); j++) {
					if(nearbyObjects.get(j)!=null) {
						if(nearbyObjects.get(j).getType().equals("vegetation")) {
							returnVal.add(Vegetation.getVegetationAt((IVector2) nearbyObjects.get(j).getProperty("position")));
						}
					}
				}
			}
			
			return returnVal.toArray(new Vegetation[returnVal.size()]);
		}

	public static Fire getFireAt(IVector2 pos) {
		for(int i=0;i<instances.size();i++){
			if (instances.get(i).getPosition().equals(pos)) {
				return instances.get(i);
			}
		}
		return null;
	}
	
	public ISpaceObject getSelf() {
		return myself;
	}
	
	public boolean deleteFire(Grid2D environment) {
		return environment.destroyAndVerifySpaceObject(myself.getId());
	}
	
	public static Fire getFireById(Integer id) {
		for(int i=0;i<instances.size();i++){
			Fire v = instances.get(i);
			if (Integer.valueOf(v.myself.getId().toString()).equals(id)) {
				return v;
			}
		}
		return null;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public static int getExtinct() {
		int amount = 0;
		for(int i=0;i<Fire.instances.size();i++) {
			if(Fire.instances.get(i).isExtinct())
				amount++;
		}
		return amount;
	}
}
