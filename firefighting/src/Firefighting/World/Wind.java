package Firefighting.World;

import java.util.HashMap;
import java.util.Map;

import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.math.IVector2;

public class Wind {
	public static Wind instance = null;
	protected ISpaceObject myself;
	
	public Wind(IVector2 direction, int speed, Grid2D env) {
		Map<Object, Object> init = new HashMap<>();
		init.put("direction", direction);
		init.put("speed", speed);
		
		myself = env.createSpaceObject("wind", init, null);
		instance = this;
	}
	
	public IVector2 getDirection() {
		return (IVector2)myself.getProperty("direction");
	}
	
	public int getSpeed() {
		return ((Integer) myself.getProperty("speed")).intValue();
	}
	
	public void setSpeed(int newSpeed) {
		myself.setProperty("speed", newSpeed);
	}
	
	public void setDirection(IVector2 newDir) {
		myself.setProperty("direction", newDir);
	}
	
	public String dirToString() {
		IVector2 dir = this.getDirection();
		if(dir.getXAsInteger()>0 && dir.getYAsInteger()==0) {
			return "East!";
		}
		else if(dir.getXAsInteger()<0 && dir.getYAsInteger()==0) {
			return "West!";
		}
		else if(dir.getXAsInteger()>0 && dir.getYAsInteger()>0) {
			return "Northeast!";
		}
		else if(dir.getXAsInteger()<0 && dir.getYAsInteger()>0) {
			return "Northwest!";
		}
		else if(dir.getXAsInteger()>0 && dir.getYAsInteger()<0) {
			return "Southeast!";
		}
		else if(dir.getXAsInteger()<0 && dir.getYAsInteger()<0) {
			return "Southwest!";
		}
		else if(dir.getXAsInteger()==0 && dir.getYAsInteger()>0) {
			return "North!";
		}
		else {
			return "South!";
		}
	}
}
