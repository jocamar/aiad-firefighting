package Firefighting.World;

import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Victim {
	public static ArrayList<Victim> instances = new ArrayList<Victim>();
	protected static Grid2D environment = null;
	protected ISpaceObject myself;
	
	public Victim(int x, int y, Grid2D env) {
		Map<Object, Object> init = new HashMap<>();
		init.put("position", new Vector2Int(x,y));
		init.put("saved", false);
		init.put("dead", false);
		init.put("endangered", false);
		
		myself = env.createSpaceObject("victim", init, null);
		
		if(environment == null)
			environment = env;
		else if (environment != env) {
			environment = env;
			instances.clear();
		}
		instances.add(this);
	}
	
	public IVector2 getPosition()
	{
		return (IVector2)myself.getProperty(Space2D.PROPERTY_POSITION);
	}
	
	public boolean isSaved()
	{
		return ((Boolean) myself.getProperty("saved")).booleanValue();
	}
	
	public boolean isEndangered()
	{
		return ((Boolean) myself.getProperty("endangered")).booleanValue();
	}
	
	public boolean isDead()
	{
		return ((Boolean) myself.getProperty("dead")).booleanValue();
	}
	
	public static Victim getVictimAt(IVector2 pos) {
		for(Victim v : instances) {
			if(v.getPosition().equals(pos)) {
				return v;
			}
		}
		
		return null;
	}
	
	public void setSaved(boolean val) {
		myself.setProperty("saved", val);
	}
	
	public void setDead(boolean val) {
		myself.setProperty("dead", val);
	}
	
	public void setEndangered(boolean val) {
		myself.setProperty("endangered", val);
	}
	
	public ISpaceObject getSelf() {
		return myself;
	}

	public static int getSaved() {
		int amount = 0;
		for(int i=0;i<Victim.instances.size();i++) {
			if(Victim.instances.get(i).isSaved())
				amount++;
		}
		return amount;
	}
	
	public static int getDead() {
		int amount = 0;
		for(int i=0;i<Victim.instances.size();i++) {
			if(Victim.instances.get(i).isDead())
				amount++;
		}
		return amount;
	}
}
