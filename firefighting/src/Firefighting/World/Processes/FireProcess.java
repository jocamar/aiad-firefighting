package Firefighting.World.Processes;

import java.util.ArrayList;
import java.util.Random;

import Firefighting.World.Fire;
import Firefighting.World.Vegetation;
import Firefighting.World.Victim;
import Firefighting.World.Wind;
import jadex.bridge.service.types.clock.IClockService;
import jadex.commons.SimplePropertyObject;
import jadex.extension.envsupport.environment.IEnvironmentSpace;
import jadex.extension.envsupport.environment.ISpaceProcess;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

public class FireProcess extends SimplePropertyObject implements ISpaceProcess {

	public static double spreadRate;
	protected double lasttick;
	public static double windInfluence;
	public static double vegInfluence;
	
	@Override
	public void execute(IClockService clock, IEnvironmentSpace space) {
		if (lasttick + 10 < clock.getTick()) {
			lasttick = clock.getTick();
			spreadFire(space);
			updateFires(space);
		}
	}

	@Override
	public void shutdown(IEnvironmentSpace space) {
		
	}
	
    public Vegetation[] getNeighbors(IVector2 firePosition) {
        ArrayList<Vegetation> vegs = new ArrayList<Vegetation>();
        int x = firePosition.getXAsInteger();
        int y = firePosition.getYAsInteger();
        
        for(int i=0;i<Vegetation.instances.size();i++) {
            int newX = Vegetation.instances.get(i).getPosition().getXAsInteger();
            int newY = Vegetation.instances.get(i).getPosition().getYAsInteger();

            if(((newX == (x+1))&&(newY == y)) || ((newX == (x-1))&&(newY == y)) || ((newY == (y+1))&&(newX == x)) || ((newY == (y-1))&&(newX == x)) ||
                ((newX == (x+1))&&(newY == (y+1))) || ((newX == (x+1))&&(newY == (y-1))) ||
                ((newX == (x-1))&&(newY == (y+1))) || ((newX == (x-1))&&(newY == (y-1)))
                    ) {
                vegs.add(Vegetation.instances.get(i));
            }
        }
        
        return vegs.toArray(new Vegetation[vegs.size()]);
    }
    
    public Victim[] getVictimNeighbors(IVector2 firePosition) {
        ArrayList<Victim> vics = new ArrayList<Victim>();
        int x = firePosition.getXAsInteger();
        int y = firePosition.getYAsInteger();
        
        for(int i=0;i<Victim.instances.size();i++) {
            int newX = Victim.instances.get(i).getPosition().getXAsInteger();
            int newY = Victim.instances.get(i).getPosition().getYAsInteger();
            
            if(((newX == (x+1))&&(newY == y)) || ((newX == (x-1))&&(newY == y)) || ((newY == (y+1))&&(newX == x)) || ((newY == (y-1))&&(newX == x)) ||
                ((newX == (x+1))&&(newY == (y+1))) || ((newX == (x+1))&&(newY == (y-1))) ||
                ((newX == (x-1))&&(newY == (y+1))) || ((newX == (x-1))&&(newY == (y-1)))
                    ) {
            	vics.add(Victim.instances.get(i));
            }
        }
        
        return vics.toArray(new Victim[vics.size()]);
    }
	
	synchronized public void updateFires(IEnvironmentSpace space) {
		ArrayList<Fire> fires = Fire.instances;
		
		for (int i = 0; i < fires.size(); i++) {
			Fire fire = fires.get(i);
			Integer intensity = (Integer) fire.getIntensity();
			IVector2 firePosition=fire.getPosition();
			Vegetation[] vegetations = Vegetation.getAllAtPosition(firePosition);
			Vegetation vegetation = null;
			if(vegetations.length>0)
				vegetation = vegetations[0];
			
			Integer density = 0;
			if(vegetation!=null)
				density = (Integer) vegetation.getDensity();
			
			boolean extinct=fire.isExtinct();
			
			if(extinct) {
				if(Vegetation.getVegetationAt(firePosition) != null
						&& Vegetation.getVegetationAt(firePosition).getDensity()<=0
						&& !fire.isDeleted()) {
					if(!fire.deleteFire((Grid2D) space))
						System.out.println("Unable to remove fire!");
					else fire.setDeleted(true);
				}
				continue;
			}
			
			Victim victim = Victim.getVictimAt(firePosition);
			
			if(victim!=null && !victim.isSaved())
				victim.setDead(true);
						
			density-=5;
			
			if(density > 30) {
				intensity += 5;
			}
			else {
				if(density <0)
					density = 0;
				intensity -= 5;
			}
			
			if(intensity <= 0) {
				intensity = 0;
				density = 0;
				fire.setExtinguished(true);
			}
			
			fire.setIntensity(intensity);
			if(vegetation!=null)
				vegetation.setDensity(density);
		}
	}
	
	synchronized public void spreadFire(IEnvironmentSpace space) {
		Space2D env = (Space2D)space;
		Random rand = new Random();
		ArrayList<Fire> fires = null;
		Wind wind = Wind.instance;
		IVector2 direction = new Vector2Int(0,0);
		Integer speed = 0;

		fires = Fire.instances;
		if(wind != null) {
			direction = wind.getDirection();
			speed = (Integer)wind.getSpeed();

		}

		int size = fires.size();
		for (int i = 0; i < size; i++) {
			Fire fire = fires.get(i);
			IVector2 position=fire.getPosition();

			boolean extinct=(boolean)fire.isExtinct();

			if(extinct)
				continue;

            Vegetation[] vegetations = getNeighbors(position);
			determineVegProb(env, rand, direction, speed, fire, position,
					vegetations);
			
			Victim[] victims = getVictimNeighbors(position);
			determineVictimProb(env, rand, direction, speed, fire, position,
					victims);
		}
	}

	/**
	 * @param env
	 * @param rand
	 * @param direction
	 * @param speed
	 * @param fire
	 * @param position
	 * @param victims
	 */
	private void determineVictimProb(Space2D env, Random rand,
			IVector2 direction, Integer speed, Fire fire, IVector2 position,
			Victim[] victims) {
		for (int j = 0; j < victims.length; j++) {
			Victim v = victims[j];
			IVector2 vicPosition = v.getPosition();
			
			if(v.isDead() || v.isSaved()) continue;

		    if((Fire.getFireAt(vicPosition)==null || Fire.getFireAt(vicPosition).isExtinct())) {
				int difX = position.getXAsInteger() - vicPosition.getXAsInteger();
				int difY = position.getYAsInteger() - vicPosition.getYAsInteger();
				int modX = 0;
				int modY = 0;

				//to the right
				if(difX < 0 && direction.getX().getAsInteger() < 0 ||
						difX > 0 && direction.getX().getAsInteger() > 0) {
					modX = direction.getX().getAsInteger();
				}

				if(difY < 0 && direction.getY().getAsInteger() < 0 ||
						difY > 0 && direction.getY().getAsInteger() > 0) {
					modY = direction.getY().getAsInteger();
				}

				double modDir = Math.sqrt(Math.pow(modX,2)+Math.pow(modY,2));

				double windComponent = (speed*modDir)*windInfluence;

				double vegComponent = ((2*(windComponent*50))/100)*vegInfluence;

				double prob = spreadRate*(((Integer)fire.getIntensity()+vegComponent)/3);
				Integer numGen = rand.nextInt(100);

				if(prob >= numGen) {
					//Rekindle fire only if veg is not gone
		            if(Fire.getFireAt(vicPosition)!=null
		            		&& Fire.getFireAt(vicPosition).isExtinct()) {
		                Fire.getFireAt(vicPosition).setExtinguished(false);
		                Fire.getFireAt(vicPosition).setIntensity(5);
		            }
		            else
		                new Fire(vicPosition.getXAsInteger(), vicPosition.getYAsInteger(),new Integer(5), (Grid2D)env);
				}
			}
		}
	}

	/**
	 * @param env
	 * @param rand
	 * @param direction
	 * @param speed
	 * @param fire
	 * @param position
	 * @param vegetations
	 */
	private void determineVegProb(Space2D env, Random rand, IVector2 direction,
			Integer speed, Fire fire, IVector2 position,
			Vegetation[] vegetations) {
		for (int j = 0; j < vegetations.length; j++) {
			Vegetation v = vegetations[j];
			IVector2 vegPosition = v.getPosition();

		    if((Fire.getFireAt(vegPosition)==null || Fire.getFireAt(vegPosition).isExtinct()) && (v.getDensity()>0)) {
				int difX = position.getXAsInteger() - vegPosition.getXAsInteger();
				int difY = position.getYAsInteger() - vegPosition.getYAsInteger();
				int modX = 0;
				int modY = 0;

				//to the right
				if(difX < 0 && direction.getX().getAsInteger() < 0 ||
						difX > 0 && direction.getX().getAsInteger() > 0) {
					modX = direction.getX().getAsInteger();
				}

				if(difY < 0 && direction.getY().getAsInteger() < 0 ||
						difY > 0 && direction.getY().getAsInteger() > 0) {
					modY = direction.getY().getAsInteger();
				}

				double modDir = Math.sqrt(Math.pow(modX,2)+Math.pow(modY,2));

				double windComponent = (speed*modDir);

				double vegComponent = (5*(windComponent*(Integer)v.getDensity()))/100;

				double prob = spreadRate*(((Integer)fire.getIntensity()+vegComponent)/6);
				Integer numGen = rand.nextInt(100);

				if(prob >= numGen) {
					//Rekindle fire only if veg is not gone
		            if(Fire.getFireAt(vegPosition)!=null
		            		&& Fire.getFireAt(vegPosition).isExtinct()) {
		                Fire.getFireAt(vegPosition).setExtinguished(false);
		                Fire.getFireAt(vegPosition).setIntensity(5);
		            }
		            else
		                new Fire(vegPosition.getXAsInteger(), vegPosition.getYAsInteger(),new Integer(5), (Grid2D)env);
				}
			}
		}
	}

	@Override
	public void start(IClockService clock, IEnvironmentSpace space) {
		lasttick = clock.getTick();
		boolean validSpot = false;
		Space2D env = (Space2D)space;
		Random rand = new Random();
		spreadRate = Double.parseDouble(this.getProperty("spreadrate").toString());
		windInfluence = Double.parseDouble(this.getProperty("windinfluence").toString());
		vegInfluence = Double.parseDouble(this.getProperty("veginfluence").toString());
		int x, y;
		Integer intensity = rand.nextInt(100);
		
		while(!validSpot) {
			x = rand.nextInt(env.getAreaSize().getXAsInteger());
			y = rand.nextInt(env.getAreaSize().getYAsInteger());
			Vegetation veg = Vegetation.getVegetationAt(new Vector2Int(x,y));
			
			if(veg != null) {
				new Fire(x, y, intensity, (Grid2D)env);
				validSpot = true;
			}
		}
	}
}
