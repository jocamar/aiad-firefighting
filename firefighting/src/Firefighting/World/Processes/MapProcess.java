package Firefighting.World.Processes;

import java.util.Random;

import Firefighting.Squad;
import Firefighting.Util;
import Firefighting.World.Vegetation;
import Firefighting.World.Victim;
import Firefighting.World.Wind;
import jadex.bridge.service.types.clock.IClockService;
import jadex.commons.SimplePropertyObject;
import jadex.extension.envsupport.environment.IEnvironmentSpace;
import jadex.extension.envsupport.environment.ISpaceProcess;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.Vector2Int;

public class MapProcess extends SimplePropertyObject implements ISpaceProcess {

	protected double lasttick;
	
	@Override
	public void execute(IClockService clock, IEnvironmentSpace space) {
		if (lasttick + 15000 < clock.getTick()) {
			lasttick = clock.getTick();
			updateWind();
		}
	}

	/**
	 * 
	 */
	synchronized private void updateWind() {
		Random rand = new Random();
		Integer x = rand.nextInt(2)-1;
		Integer y = rand.nextInt(2)-1;
		Integer intensity = rand.nextInt(100);
		
		Wind.instance.setDirection(new Vector2Int(x,y));
		Wind.instance.setSpeed(intensity);
	}

	@Override
	public void shutdown(IEnvironmentSpace space) {
		
	}

	@Override
	public void start(IClockService clock, IEnvironmentSpace space) {
		lasttick = clock.getTick();
		Util.fighters.clear();
		Util.trucks.clear();
		Squad.squads.clear();
		Space2D env = (Space2D)space;
		
		Random rand = new Random();
		
		for (int x = 0; x < env.getAreaSize().getXAsInteger(); x++) {
			for (int y = 0; y < env.getAreaSize().getYAsInteger(); y++) {
				Integer r = rand.nextInt(5);
				
				if(r.intValue() >= 1) {
					r = rand.nextInt(100)+1;
					
					new Vegetation(x, y, r.intValue(), (Grid2D)env);
				} else {
					r = rand.nextInt(30);
					if(r.intValue() <= 1) {
						new Victim(x,y,(Grid2D)env);
					}
				}
			}
		}
		
		Integer x = rand.nextInt(2)-1;
		Integer y = rand.nextInt(2)-1;
		Integer intensity = rand.nextInt(100);
		
		new Wind(new Vector2Int(x,y), intensity, (Grid2D) env);
	}

}
