package Firefighting;

import java.util.Collection;
import java.util.Iterator;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.PlanFailureException;
import jadex.commons.future.DefaultResultListener;
import jadex.commons.future.IFuture;
import jadex.extension.envsupport.math.IVector2;

/**
 *  Go to a specified position.
 */
@Plan
public class RequestSupplyPlanEnv 
{
    //-------- attributes --------

    @PlanCapability
    protected FirefighterBDI fighter;
    
    @PlanAPI
    protected IPlan rplan;
    
    protected int action = -1;
    
    /**
     *  The plan body.
     */
    @PlanBody
    public void body()
    {
        if(!fighter.isDead()) {
            IVector2 pos = fighter.getPosition();
            
            if(pos!=null) {
                final String msg = pos.getXAsInteger() + "-" + pos.getYAsInteger() + "-" + fighter.getSquad().getId() + "-" + fighter.getSquad().getAssignedQuad().getId() + "-" + "requestsupply";
                
                IFuture<Collection<IChatService>> chatservices = fighter.getAgent().getRequiredServices("chatservices");
                chatservices.addResultListener(new DefaultResultListener<Collection<IChatService>>()
                {
                    public void resultAvailable(Collection<IChatService> result)
                    {
                        for(Iterator<IChatService> it=result.iterator(); it.hasNext(); ) {
                            IChatService cs = it.next();
                            cs.message(fighter.getAgent().getComponentIdentifier().getLocalName(), msg);
                        }
                    }
                });
            }
        }
    	throw new PlanFailureException();
    }
    
    @PlanAborted
    public void  aborted()
    {
        
    }
}