package Firefighting;

import java.util.ArrayList;

import Firefighting.World.Fire;
import Firefighting.World.Victim;

public class Quad {
	public static int idquad = 0;
	private double startX;
	private double startY;
	private double length;
	private double height;
	private double danger;
	private double squadsAssigned;
	private int id;
	
	public Quad(double startX, double startY, double length, double height, double danger) {
		this.startX = startX;
		this.startY = startY;
		this.length = length;
		this.height = height;
		this.danger = danger;
		id = idquad;
		idquad++;
	}
	
	public double getStartX() {
		return startX;
	}
	public void setStartX(double startX) {
		this.startX = startX;
	}
	public double getStartY() {
		return startY;
	}
	public void setStartY(double startY) {
		this.startY = startY;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getDanger() {
		return danger;
	}
	public void setDanger(double danger) {
		if(danger<0)
			this.danger = 0;
		else
			this.danger = danger;
	}
	
	synchronized public Fire findWorstFire() {
		Fire worstFire = null;
		double worstInt = 0;
		for(int j=0;j<Fire.instances.size();j++) {
			Fire fire = Fire.instances.get(j);
			
			if(fire.getPosition().getXAsDouble() >= this.getStartX() &&
					fire.getPosition().getXAsDouble() <= (this.getStartX()+this.getLength()) &&
					fire.getPosition().getYAsDouble() >= this.getStartY() &&
					fire.getPosition().getYAsDouble() <= (this.getStartY()+this.getHeight())) {
				if(fire.getIntensity()>worstInt && !fire.isTackled()) {
					worstInt = fire.getIntensity()*CommanderBDI.dangerPerFire;
					worstFire = fire;
				}
			}
		}
		
		return worstFire;
	}
	
	synchronized public ArrayList<Victim> getEndageredVictims() {
		ArrayList<Victim> victims = this.getVictims();
		ArrayList<Victim> victimsAtRisk = new ArrayList<Victim>();
		
		for(int j=0;j<victims.size();j++) {
			Victim victim = victims.get(j);
			if(victim.isEndangered() && !victim.isDead() && !victim.isSaved()) {
				victimsAtRisk.add(victim);
			}
		}
		
		return victimsAtRisk;
	}
	
	synchronized public static Quad getQuadById(int id) {
		for(int j=0;j<CommanderBDI.quads.size();j++) {
			if(CommanderBDI.quads.get(j).getId()==id) {
				return CommanderBDI.quads.get(j);
			}
		}
		return null;
	}
	
	synchronized public ArrayList<Victim> getVictims() {
		ArrayList<Victim> victims = new ArrayList<Victim>();
		for(int j=0;j<Victim.instances.size();j++) {
			Victim victim = Victim.instances.get(j);
			if(victim.getPosition().getXAsDouble() >= this.getStartX() &&
					victim.getPosition().getXAsDouble() <= (this.getStartX()+this.getLength()) &&
					victim.getPosition().getYAsDouble() >= this.getStartY() &&
					victim.getPosition().getYAsDouble() <= (this.getStartY()+this.getHeight())) {
				victims.add(victim);
			}
		}
		return victims;
	}
	
	synchronized public boolean hasSquadAssigned() {
		for(int i=0;i<Squad.squads.size();i++) {
			if(Squad.squads.get(i).getAssignedQuad() != null && Squad.squads.get(i).getAssignedQuad().equals(this)) {
				return true;
			}
		}
		return false;
	}

	synchronized public double getSquadsAssigned() {
		return squadsAssigned;
	}

	synchronized public void setSquadsAssigned(double squadsAssigned) {
		if(squadsAssigned<0) {
			this.squadsAssigned = 0;	
		}
		else {
			this.squadsAssigned = squadsAssigned;
		}
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
};
