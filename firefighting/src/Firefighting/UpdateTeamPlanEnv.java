package Firefighting;

import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.PlanFailureException;

/**
 *  Send a unit to a specific location
 */
@Plan
public class UpdateTeamPlanEnv {
	
	@PlanCapability
	protected CommanderBDI agent;
	
	@PlanAPI
	protected IPlan rplan;
	
	protected int action = -1;
	
	/**
	 *  The plan body.
	 */
	@PlanBody
	synchronized public void body()
	{
		if(agent.isFinished())
			return;
		
		agent.updateQuads();
		if(!agent.getDirection().equalsIgnoreCase(agent.getWind().dirToString())) {
			System.out.println("There is now a wind blowing " + agent.getWind().dirToString() + " with an intensity of " + agent.getWind().getSpeed());
			agent.setDirection(agent.getWind().dirToString());
		}
		
		for(int i=0; i<agent.getSquads().size(); i++) {
			Squad s = agent.getSquads().get(i);
			int fightersAlive = 0;
			int trucksAlive = 0;
			for(int j=0;j<s.getMembers().size(); j++) {
				BaseAgentBDI agent = s.getMembers().get(j);
				
				if(agent.isDead())
					continue;
				
				if(agent.getAgent().getComponentIdentifier().getLocalName().contains("Firefighter"))
					fightersAlive++;
				if(agent.getAgent().getComponentIdentifier().getLocalName().contains("Firetruck"))
					trucksAlive++;
			}
			
			if((fightersAlive==0 && trucksAlive>0)
					|| (fightersAlive>0 && trucksAlive==0)) {
				System.out.println("Agents found alone; reassigning.");
				
				Squad mostEndangered = null;
				double highestDanger = 0;
				for(int k=0; k<agent.getSquads().size(); k++) {
					Squad s2 = agent.getSquads().get(k);
					
					if(s2.equals(s))
						continue;
					
					double currDanger = s2.getAssignedQuad().getDanger();
					
					if(mostEndangered==null || currDanger > highestDanger) {
						mostEndangered = s2;
						highestDanger = currDanger;
					}
				}
				
				if(mostEndangered!=null) {
					for(int j=0;j<s.getMembers().size(); j++) {
						BaseAgentBDI agent = s.getMembers().get(j);
						
						if(agent.isDead())
							continue;
						
						System.out.println("Agent reassigned to squad " + mostEndangered.getId());
						agent.setSquad(mostEndangered);
					}
				}
			}
			else if(fightersAlive==0 && trucksAlive==0) {
				System.out.println("No members left live; destroying squad.");
				agent.removeSquad(s);
				i--;
			}
		}
		
		for(int k=0; k<agent.getSquads().size(); k++) {
			Squad s = agent.getSquads().get(k);
			
			if(s.getMembers().size()<=0) {
				agent.removeSquad(s);
				k--;
			}
		}
		throw new PlanFailureException();
	}

	@PlanAborted
	public void	aborted()
	{
		
	}	
}
