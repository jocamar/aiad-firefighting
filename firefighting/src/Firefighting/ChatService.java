package Firefighting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import jadex.bdiv3.BDIAgent;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.ServiceComponent;
import jadex.commons.future.DefaultResultListener;
import jadex.commons.future.IFuture;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

@Service
public class ChatService implements IChatService {

	@ServiceComponent
	protected BDIAgent agent;
	
	@Override
	public void message(final String sender, final String text) {
		int id = Integer.parseInt(text.split("-")[2]);
		int idQuad = Integer.parseInt(text.split("-")[3]);
		String opType = text.split("-")[4];
		
        if (agent.getComponentIdentifier().getLocalName().contains("Firefighter") && !opType.equalsIgnoreCase("requestsupply")) {
			for(int i=0;i<Util.fighters.size();i++) {
				if(Util.fighters.get(i).getAgent().equals(agent)&&id==Util.fighters.get(i).getSquad().getId()) {
					
                    if(!opType.equalsIgnoreCase("requestsupply") && !opType.equalsIgnoreCase("comecloser")) {
						if(Util.fighters.get(i).getSquad().getAssignedQuad() == null || !Util.fighters.get(i).getSquad().getAssignedQuad().equals(Quad.getQuadById(idQuad))) {
							Util.fighters.get(i).getSquad().setAssignedQuad(Quad.getQuadById(idQuad));
							Quad.getQuadById(idQuad).setDanger(Quad.getQuadById(idQuad).getDanger()-25);
						}
						
						IVector2 destination = new Vector2Int(Integer.parseInt(text.split("-")[0]),
																Integer.parseInt(text.split("-")[1]));
						
						if((opType.equalsIgnoreCase("extinguish") || (opType.equalsIgnoreCase("save"))))
								agent.dispatchTopLevelGoal(new FollowCommand(destination,Util.fighters.get(i))).get();
					}
                    else {
                        IVector2 destination = new Vector2Int(Integer.parseInt(text.split("-")[0]),
                                Integer.parseInt(text.split("-")[1]));
                        
                        agent.dispatchTopLevelGoal(new FollowCommand(destination,Util.fighters.get(i))).get();
                    }

					break;
				}
			}
        } else if(agent.getComponentIdentifier().getLocalName().contains("Firetruck") && !opType.equalsIgnoreCase("save") && !opType.equalsIgnoreCase("comecloser")) {
			for(int i=0;i<Util.trucks.size();i++) {
				if(Util.trucks.get(i).getAgent().equals(agent)&&id==Util.trucks.get(i).getSquad().getId()) {
					final FiretruckBDI truck = Util.trucks.get(i);
					IVector2 destination = null;
					
                    if(!opType.equalsIgnoreCase("requestsupply")) {
                        if(truck.getSquad().getAssignedQuad()==null || !truck.getSquad().getAssignedQuad().equals(Quad.getQuadById(idQuad))) {
                            truck.getSquad().setAssignedQuad(Quad.getQuadById(idQuad));
                            Quad.getQuadById(idQuad).setDanger(Quad.getQuadById(idQuad).getDanger()-25);
                        }
                        
                        destination = new Vector2Int(Integer.parseInt(text.split("-")[0]), Integer.parseInt(text.split("-")[1]));
                        
                        if(opType.equalsIgnoreCase("extinguish"))
                            agent.dispatchTopLevelGoal(new FollowCommand(destination,truck)).get();
                        
                    }
                    else {
                        ArrayList<IVector2> bestPositions = truck.getSquad().getBestSupplyPosition();
                        for(int j=0;j<bestPositions.size();) {
                            destination=bestPositions.get(0);
                            break;
                        }
                        if(destination!=null && !truck.getPosition().equals(destination)) {

                            agent.dispatchTopLevelGoal(new FollowCommand(destination,truck)).get();
                        }
                        else {

                            final String msg = truck.getPosition().getXAsInteger() + "-" + truck.getPosition().getYAsInteger() + "-" + truck.getSquad().getId() + "-" + truck.getSquad().getAssignedQuad().getId() + "-" + "comecloser";
                            
                            IFuture<Collection<IChatService>> chatservices = truck.getAgent().getRequiredServices("chatservices");
                            chatservices.addResultListener(new DefaultResultListener<Collection<IChatService>>()
                            {
                                public void resultAvailable(Collection<IChatService> result)
                                {
                                    for(Iterator<IChatService> it=result.iterator(); it.hasNext(); ) {
                                        IChatService cs = it.next();
                                        cs.message(truck.getAgent().getComponentIdentifier().getLocalName(), msg);
                                    }
                                }
                            });
                        }
                    }
					break;
				}
			}
		} else if (agent.getComponentIdentifier().getLocalName().contains("Commander")) {
			if(Integer.valueOf(CommanderBDI.instance.getMyself().getId().toString()).equals(id)) {
			}
		}
	}
}
