package Firefighting;

import jadex.bdiv3.annotation.Body;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.annotation.Plans;
import jadex.bridge.service.types.clock.IClockService;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import jadex.bdiv3.annotation.Deliberation;
import jadex.bdiv3.annotation.GoalRecurCondition;


@Agent

@Plans(
{
    @Plan(trigger=@Trigger(goals=FiretruckBDI.SupplyWater.class), body=@Body(SupplyWaterPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=BaseAgentBDI.AssureSafety.class), body=@Body(AssureSafetyPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=FollowCommand.class), body=@Body(FollowCommandPlanEnv.class))
})
@ProvidedServices(@ProvidedService(type=IChatService.class, implementation=@Implementation(ChatService.class)))
@RequiredServices(
{
	@RequiredService(name="clockservice", type=IClockService.class, binding=@Binding(scope=Binding.SCOPE_PLATFORM)),
	@RequiredService(name="chatservices", type=IChatService.class, multiple=true, binding=@Binding(dynamic=true, scope=Binding.SCOPE_PLATFORM))
})
public class FiretruckBDI extends BaseAgentBDI {
	
	@AgentBody
	synchronized public void body()
	{
		fires.clear();
		victims.clear();
		dmgPerFlame = Double.parseDouble(this.getAgent().getArgument("dmgperflame").toString());
		fieldOfView = Integer.parseInt(this.getAgent().getArgument("fieldofview").toString());
		agent.dispatchTopLevelGoal(new AssureSafety());
        agent.dispatchTopLevelGoal(new SupplyWater());
		Util.trucks.add(this);
		this.setDead(false);
		this.getMyself().setProperty("position", new Vector2Int(1,2));
	}
	
	
	/** ------------------- Goals -------------------------*/
    /**
     *  Goal for supplying the firefighters with water.
     */
    @Goal(recur=true, deliberation=@Deliberation(cardinalityone=true, inhibits=SupplyWater.class))
    public class SupplyWater
    {
        @GoalRecurCondition(beliefs="currenttime")
        public boolean checkRecur() 
        {
            return true;
        }
    }

	public static int getDead() {
		int amount = 0;
		for(int i=0;i<Util.trucks.size();i++) {
			if(Util.trucks.get(i).isDead())
				amount++;
		}
		return amount;
	}


	public static FiretruckBDI getFiretruckAt(IVector2 iVector2) {
		for(int i=0;i<Util.trucks.size();i++) {
			if(Util.trucks.get(i).getPosition().equals(iVector2))
				return Util.trucks.get(i);
		}
		return null;
	}
}
