package Firefighting;

import jadex.bdiv3.annotation.Belief;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.Body;
import jadex.bdiv3.annotation.Deliberation;
import jadex.bdiv3.annotation.Goal.ExcludeMode;
import jadex.bdiv3.annotation.GoalMaintainCondition;
import jadex.bdiv3.annotation.GoalRecurCondition;
import jadex.bdiv3.annotation.GoalTargetCondition;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Plans;
import jadex.bdiv3.annotation.Trigger;
import jadex.bridge.service.types.clock.IClockService;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.Implementation;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

@Agent

@Plans(
{
	@Plan(trigger=@Trigger(goals=FirefighterBDI.ExtinguishFire.class), body=@Body(ExtinguishFirePlanEnv.class)),
	@Plan(trigger=@Trigger(goals=FirefighterBDI.AssureSafety.class), body=@Body(AssureSafetyPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=FirefighterBDI.ShootWater.class), body=@Body(ShootWaterPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=FirefighterBDI.SaveVictim.class), body=@Body(SaveVictimPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=FirefighterBDI.Rescue.class), body=@Body(RescuePlanEnv.class)),
    @Plan(trigger=@Trigger(goals=FirefighterBDI.RequestSupply.class), body=@Body(RequestSupplyPlanEnv.class)),
	@Plan(trigger=@Trigger(goals=FollowCommand.class), body=@Body(FollowCommandPlanEnv.class))
})
@ProvidedServices(@ProvidedService(type=IChatService.class, implementation=@Implementation(ChatService.class)))
@RequiredServices(
{
	@RequiredService(name="clockservice", type=IClockService.class, binding=@Binding(scope=Binding.SCOPE_PLATFORM)),
	@RequiredService(name="chatservices", type=IChatService.class, multiple=true, binding=@Binding(dynamic=true, scope=Binding.SCOPE_PLATFORM))
})
public class FirefighterBDI extends BaseAgentBDI {	
	
	@Belief(updaterate=1000)
	protected int numVictimsEndangered = updateEndangeredVictims();

    @Belief(updaterate=100)
    protected boolean supplied = checkTruckProximity();

	public static int supplyRange;

	public static double waterEffectiveness;
	
	
	
	
	/** ------------------- Body -------------------------*/
	
	/**
	 *  The agent body.
	 */
	@AgentBody
	synchronized public void body()
	{
		fires.clear();
		victims.clear();
		dmgPerFlame = Double.parseDouble(this.getAgent().getArgument("dmgperflame").toString());
		fieldOfView = Integer.parseInt(this.getAgent().getArgument("fieldofview").toString());
		supplyRange = Integer.parseInt(this.getAgent().getArgument("supplyrange").toString());
		fireDangerRange = Integer.parseInt(this.getAgent().getArgument("firedangerrange").toString());
		waterEffectiveness = Double.parseDouble(this.getAgent().getArgument("watereffectiveness").toString());
		agent.dispatchTopLevelGoal(new ExtinguishFire());
		agent.dispatchTopLevelGoal(new SaveVictim());
		agent.dispatchTopLevelGoal(new AssureSafety());
		Util.fighters.add(this);
		this.setDead(false);
		this.setSupplied(false);
		this.getMyself().setProperty("position", new Vector2Int(1,2));
	}
	
	/** ------------------- Goals -------------------------*/
	

	/**
	 *  Goal for extinguishing fire. Tries endlessly to put out fires.
	 */
    @Goal(recur=true,
            deliberation=@Deliberation(inhibits={RequestSupplyPlanEnv.class}, cardinalityone=true))
	public class ExtinguishFire
	{
		/**
		 * 
		 */
		@GoalRecurCondition(beliefs="currenttime")
		public boolean checkRecur() 
		{
			return true;
		}
	}
	
	@Goal(recur=true,
			deliberation=@Deliberation(inhibits={ExtinguishFirePlanEnv.class,
					ShootWaterPlanEnv.class,
                    RequestSupplyPlanEnv.class,
					AssureSafety.class}, cardinalityone=true))
	public class Rescue
	{
		/**
		 * 
		 */
		@GoalRecurCondition(beliefs="currenttime")
		public boolean checkRecur() 
		{
			return true;
		}
	}
	
	/**
	 *  Goal for rescuing victims. Tries endlessly to rescue victims.
	 */
	@Goal(recur=true, retrydelay=100,
			deliberation=@Deliberation(inhibits={ExtinguishFirePlanEnv.class,
			ShootWaterPlanEnv.class,
			RequestSupplyPlanEnv.class,
			AssureSafety.class}, cardinalityone=true))
	public class SaveVictim
	{
		@GoalMaintainCondition(beliefs="numVictimsEndangered")
		protected boolean maintain() {
			return numVictimsEndangered<=0;
		}
	}
	
	/**
	 *  
	 */
    @Goal(recur=true,deliberation=@Deliberation(inhibits={RequestSupplyPlanEnv.class}, cardinalityone=true))
	public class ShootWater
	{
		/**
		 * 
		 */
		@GoalRecurCondition(beliefs="currenttime")
		public boolean checkRecur() 
		{
			return true;
		}
	}
    
    /**
     *  
     */
    @Goal(excludemode=ExcludeMode.Never, retrydelay=100)
    public class RequestSupply
    {
        @GoalMaintainCondition(beliefs="currenttime")
        protected boolean maintain() {
            return checkTruckProximity();
        }
 
        @GoalTargetCondition(beliefs="currenttime")
        protected boolean target() {
            return checkTruckProximity();
        }
    }
	
	public boolean checkTruckProximity() {
		IVector2 pos = this.getPosition();
		
		if(pos==null)
			return false;
		
		int xI = pos.getXAsInteger()-supplyRange;
		if(xI<0)
			xI=0;
		int yI = pos.getYAsInteger()-supplyRange;
		if(yI<0)
			yI=0;
		int xF = pos.getXAsInteger()+supplyRange;
		if(xF>(this.getEnvironment().getAreaSize().getXAsInteger()-1))
				xF=this.getEnvironment().getAreaSize().getXAsInteger()-1;
		int yF = pos.getYAsInteger()+supplyRange;
		if(yF>(this.getEnvironment().getAreaSize().getYAsInteger()-1))
			yF=this.getEnvironment().getAreaSize().getYAsInteger()-1;
		
		for(int i=0;i<Util.trucks.size();i++) {
			FiretruckBDI truck = Util.trucks.get(i);
			if(truck.getPosition().getXAsDouble() >= xI &&
					truck.getPosition().getXAsDouble() <= xF &&
					truck.getPosition().getYAsDouble() >= yI &&
					truck.getPosition().getYAsDouble() <= yF) {
				return true;
			}
		}
		
		return false;
	}
	
	private int updateEndangeredVictims() {
		if(this.getSquad() != null && this.getSquad().getAssignedQuad() != null) {
			return this.getSquad().getAssignedQuad().getEndageredVictims().size();
		}
		else return 0;
	}
	
	public boolean isSupplied() {
		return ((Boolean) myself.getProperty("supplied")).booleanValue();
	}
	
	public void setSupplied(boolean val) {
        supplied=val;
		myself.setProperty("supplied", val);
	}


	public static int getDead() {
		int amount = 0;
		for(int i=0;i<Util.fighters.size();i++) {
			if(Util.fighters.get(i).isDead())
				amount++;
		}
		return amount;
	}
}
