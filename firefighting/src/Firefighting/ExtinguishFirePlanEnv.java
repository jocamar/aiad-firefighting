package Firefighting;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import Firefighting.MovementCapability.GoToPos;
import Firefighting.FirefighterBDI.ShootWater;
import Firefighting.World.Fire;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanAborted;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.PlanFailureException;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.SpaceObject;
import jadex.extension.envsupport.environment.space2d.Grid2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.extension.envsupport.math.Vector2Int;

/**
 *  Go to a specified position.
 */
@Plan
public class ExtinguishFirePlanEnv 
{
	//-------- attributes --------

	@PlanCapability
	protected FirefighterBDI fighter;
	
	@PlanAPI
	protected IPlan rplan;
	
	protected int action = -1;
	
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		if(!fighter.isDead()) {
			IVector2 firePos = null;
			if(fighter.getFireTarget() == null || fighter.getFireTarget().isExtinct()) {
				firePos = getClosestActiveFire();
			}
			else {
				firePos = fighter.getFireTarget().getPosition();
			}
			
			if(firePos != null) {
				IVector2 newPos = getClosestSafePos(firePos);
				
				if(newPos != null) {
					GoToPos go = fighter.getMoveCapa().new GoToPos(newPos);
					rplan.dispatchSubgoal(go).get();
					ShootWater shoot = fighter.new ShootWater();
					rplan.dispatchSubgoal(shoot).get();
				}
			}
		}
	
		throw new PlanFailureException();
	}
	
	private IVector2 getClosestActiveFire() {
		int size = fighter.getFires().size();
		List<Fire> fires = fighter.getFires();
		
		Fire best = null;
		double topPriority = 0;
		for(int i = 0; i < size; i++) {
			Fire fire = fires.get(i);
			if(fire.isExtinct())
				continue;
			
			if(best == null) {
				best = fire;
				topPriority = 1 - fire.targetNum*100 - fire.getPosition().getDistance(fighter.getPosition()).getAsDouble();
			}
			else {
				double priority = 0;
			
				priority = 1 - fire.targetNum*100 - fire.getPosition().getDistance(fighter.getPosition()).getAsDouble();
				if ( priority > topPriority) {
					best = fire;
					topPriority = priority;
				}
			}
		}
		
		if(best == null)
			return null;
		else {
			if(fighter.getFireTarget()!=null)
				fighter.getFireTarget().setTackled(false);
			fighter.setFireTarget(best);
			return best.getPosition();
		}
	}
	

	@SuppressWarnings("unchecked")
	private IVector2 getClosestSafePos(IVector2 firePos) {
		Grid2D env = (Grid2D)fighter.getEnvironment();
		IVector2 size = env.getAreaSize();
		
		int y = firePos.getYAsInteger() - 4;
		if(y < 0)
			y = 0;
		
		int x = firePos.getXAsInteger() - 4;
		if(x < 0)
			x = 0;
		
		IVector2 safePos = null;
		double curPriority = 0;
		for(int i = y; i < size.getYAsInteger() && i < firePos.getYAsInteger()+4; i++) {
			for(int j = x; j < size.getXAsInteger() && j < firePos.getXAsInteger()+4; j++) {
				IVector2 pos = new Vector2Int(j,i);
				String[] typesAgents = {"firefighter", "firetruck"};
				
				Collection<ISpaceObject> agents = env.getSpaceObjectsByGridPosition(pos,typesAgents);
				if(agents != null && agents.size() > 0)
					continue;
				
				String[] types = {"fire"};
				Set<SpaceObject> objects = env.getNearGridObjects(pos, 2, types);
				boolean safe = true;
				for(SpaceObject obj : objects) {
					if(!((Boolean) obj.getProperty("extinct")).booleanValue())
						safe = false;
				}
				
				//checks if there are any trucks near the position
				IVector2[] truckPos = Util.getNeighbors(env, pos, 3);
				String[] truck = {"truck"};
				ArrayList<SpaceObject> firetrucks = new ArrayList<SpaceObject>();
				for(IVector2 position : truckPos) {
					try {
					firetrucks.addAll((Collection<? extends SpaceObject>) env.getSpaceObjectsByGridPosition(position, truck));
					} catch(Exception e) {};
				}
				
				//calculates priority based on proximity and trucks
				double priority = -pos.getDistance(firePos).getAsDouble();
				
				for(@SuppressWarnings("unused") SpaceObject obj : firetrucks) {
					priority += 2;
				}
				
				if(safe) {
					if(safePos == null) {
						safePos = pos;
						curPriority = priority;
					}
					else if (curPriority < priority) {
						safePos = pos;
						curPriority = priority;
					}
				}
			}
		}
		return safePos;
	}

	@PlanAborted
	public void	aborted()
	{
		
	}
}

