package Firefighting;

import jadex.bdiv3.BDIAgent;
import jadex.bdiv3.annotation.Capability;
import jadex.bdiv3.annotation.Deliberation;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.Plans;
import jadex.bdiv3.annotation.Trigger;
import jadex.bdiv3.annotation.Body;
import jadex.bdiv3.annotation.Goal.ExcludeMode;
import jadex.bdiv3.runtime.ICapability;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Space2D;
import jadex.extension.envsupport.math.IVector2;
import jadex.micro.annotation.Agent;


@Capability
@Plans(@Plan(trigger=@Trigger(goals={MovementCapability.GoToPos.class}), body=@Body(GoPlanEnv.class)))
public class MovementCapability implements IEnvAccess {

	/** The capability. */
	@Agent
	protected ICapability capa;
	
	/** The environment. */
	protected Space2D env = (Space2D)capa.getAgent().getParentAccess().getExtension("myff2dspace").get();
	
	/** The environment. */
	protected ISpaceObject myself = env.getAvatar(capa.getAgent().getComponentDescription(), capa.getAgent().getModel().getFullName());

	/** The home position (=first position). */
	protected IVector2 homepos = myself!=null? (IVector2)myself.getProperty(Space2D.PROPERTY_POSITION): null;
	
	/**
	 *  Goal for going to a specified position.
	 */
	@Goal(excludemode=ExcludeMode.Never, deliberation=@Deliberation(inhibits=GoPlanEnv.class, cardinalityone=true))
	public class GoToPos implements IGoToPos
	{
		/** The position. */
		protected IVector2 pos;
		/**
		 *  Create a new Go. 
		 */
		public GoToPos(IVector2 pos)
		{
			this.pos = pos;
		}
		
		/**
		 *  Get the pos.
		 *  @return The pos.
		 */
		public IVector2 getPosition()
		{
			return pos;
		}
	}
	
	public IVector2 getPosition()
	{
		return (IVector2)getMyself().getProperty("position");
	}

	/**
	 * 
	 */
	public IVector2 getHomePosition()
	{
		return homepos;
	}
	
	/**
	 *  Get the env.
	 *  @return The env.
	 */
	public Space2D getEnvironment()
	{
		return env;
	}

	/**
	 *  Get the myself.
	 *  @return The myself.
	 */
	public ISpaceObject getMyself()
	{
		return myself;
	}

	/**
	 *  Get the capa.
	 *  @return The capa.
	 */
	public ICapability getCapability()
	{
		return capa;
	}
	
	@Override
	public BDIAgent getAgent() {
		return capa.getAgent();
	}
}
