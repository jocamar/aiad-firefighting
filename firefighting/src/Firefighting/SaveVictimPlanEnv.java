package Firefighting;

import java.util.ArrayList;

import Firefighting.FirefighterBDI.Rescue;
import Firefighting.MovementCapability.GoToPos;
import Firefighting.World.Fire;
import Firefighting.World.Victim;
import jadex.bdiv3.annotation.Plan;
import jadex.bdiv3.annotation.PlanAPI;
import jadex.bdiv3.annotation.PlanBody;
import jadex.bdiv3.annotation.PlanCapability;
import jadex.bdiv3.runtime.IPlan;
import jadex.bdiv3.runtime.impl.PlanFailureException;
import jadex.extension.envsupport.math.IVector2;

@Plan
public class SaveVictimPlanEnv {
	@PlanCapability
	protected FirefighterBDI fighter;

	@PlanAPI
	protected IPlan rplan;
	
	/**
	 *  The plan body.
	 */
	@PlanBody
	public void body()
	{
		if(!fighter.isDead()) {
			ArrayList<Victim> victims = fighter.getSquad().getAssignedQuad().getEndageredVictims();
			fighter.setVictimTarget(getBestVictim(victims));
	
			if(fighter.getVictimTarget()!=null) {
				GoToPos go = fighter.getMoveCapa().new GoToPos(fighter.getVictimTarget().getPosition());
				rplan.dispatchSubgoal(go).get();
				Rescue rescue = fighter.new Rescue();
				rplan.dispatchSubgoal(rescue).get();
			}
		}		
		throw new PlanFailureException();
	}
	
	synchronized private Victim getBestVictim(ArrayList<Victim> victims) {
		Victim bestVictim = null;
		double bestPriority = 0;
		
		for(int i=0;i<victims.size();i++) {
			Victim currVictim = victims.get(i);
			double currPriority = 0;
			
			currPriority -= currVictim.getPosition().getDistance(fighter.getPosition()).getAsDouble()*CommanderBDI.distanceInfluence;
			currPriority += calculateDanger(currVictim.getPosition());
			if(currVictim.isEndangered())
				currPriority+= 200*CommanderBDI.endangeredInfluence;
			
			if(bestVictim!=null) {
				if(bestPriority<currPriority) {
					bestVictim = currVictim;
					bestPriority = currPriority;
				}
			}
			else {
				bestVictim = currVictim;
				bestPriority = currPriority;
			}
		}
		
		return bestVictim;
		
	}
	
	private int calculateDanger(IVector2 pos) {
		int fires = 0;
		int rtnVal = 0;
		int size = fighter.fires.size();
		if(pos != null) {
			for(int i = 0; i < size; i++) {
				Fire f = fighter.fires.get(i);
				if(f.getPosition().getDistance(pos).getAsDouble()<CommanderBDI.tilesToDanger) {
					fires++;
					rtnVal += f.getIntensity()*CommanderBDI.dangerPerFire;
				}
			}
		}
		
		if(fires==0)
			return rtnVal;
		else {
			rtnVal = rtnVal/fires;
		
			return rtnVal;
		}
	}
}