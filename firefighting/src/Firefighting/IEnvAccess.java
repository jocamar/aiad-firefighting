package Firefighting;

import jadex.bdiv3.BDIAgent;
import jadex.extension.envsupport.environment.ISpaceObject;
import jadex.extension.envsupport.environment.space2d.Space2D;

public interface IEnvAccess {
	/**
	 *  Get the env.
	 *  @return The env.
	 */
	public Space2D getEnvironment();

	/**
	 *  Get the myself.
	 *  @return The myself.
	 */
	public ISpaceObject getMyself();

	public BDIAgent getAgent();
}
