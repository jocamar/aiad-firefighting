package Firefighting;

import jadex.bdiv3.annotation.Deliberation;
import jadex.bdiv3.annotation.Goal;
import jadex.bdiv3.annotation.GoalDropCondition;
import jadex.bdiv3.annotation.Goal.ExcludeMode;
import jadex.extension.envsupport.math.IVector2;

@Goal(excludemode=ExcludeMode.Never,
		deliberation=@Deliberation(inhibits={ExtinguishFirePlanEnv.class,
												ShootWaterPlanEnv.class,
                                                SupplyWaterPlanEnv.class,
                                                RequestSupplyPlanEnv.class}, cardinalityone=true))

public class FollowCommand
{
	private IVector2 dest;
	private BaseAgentBDI agent;
	
	public FollowCommand(IVector2 dest,BaseAgentBDI agent) {
		this.setDest(dest);
		this.setAgent(agent);
	}

	public IVector2 getDest() {
		return dest;
	}

	public void setDest(IVector2 dest) {
		this.dest = dest;
	}
	
	@GoalDropCondition()
	public boolean checkDrop()
	{
		boolean ret = agent.getPosition().equals(dest);
		return ret;
	}

	public BaseAgentBDI getAgent() {
		return agent;
	}

	public void setAgent(BaseAgentBDI agent) {
		this.agent = agent;
	}
}