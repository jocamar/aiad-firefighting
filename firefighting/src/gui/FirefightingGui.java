package gui;

import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import Firefighting.BaseAgentBDI;
import Firefighting.CommanderBDI;
import Firefighting.FirefighterBDI;
import Firefighting.Util;
import Firefighting.World.Processes.FireProcess;

public class FirefightingGui extends JPanel implements ChangeListener, ActionListener{
    
    static final int PERCENTAGE_MIN = 0;
    static final int PERCENTAGE_MAX = 10;
    static final int PERCENTAGE_INIT = 10;
    JButton toggleGoalButton;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public FirefightingGui()
	{
		super();
		setLayout(new GridLayout(8,8));
		
		JLabel spreadRateLabel = new JLabel("Spread Rate", JLabel.CENTER);
		spreadRateLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel windInfluenceLabel = new JLabel("Wind Influence", JLabel.CENTER);
		windInfluenceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel vegInfluenceLabel = new JLabel("Vegetation Influence", JLabel.CENTER);
		vegInfluenceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel waterEffectivenessLabel = new JLabel("Water Effectiveness", JLabel.CENTER);
		waterEffectivenessLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel dangerPerFireLabel = new JLabel("Danger Per Fire", JLabel.CENTER);
		dangerPerFireLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel dangerPerVegLabel = new JLabel("Danger Per Vegetation", JLabel.CENTER);
		dangerPerVegLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel businessInfluenceLabel = new JLabel("Business Influence", JLabel.CENTER);
		businessInfluenceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel distanceInfluenceLabel = new JLabel("Distance Influence", JLabel.CENTER);
		distanceInfluenceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel endangeredInfluenceLabel = new JLabel("Endangered Influence", JLabel.CENTER);
		endangeredInfluenceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel squadDangerInfluenceLabel = new JLabel("Squad Danger Influence", JLabel.CENTER);
		squadDangerInfluenceLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JLabel damagePerFlameLabel = new JLabel("Damage Per Second", JLabel.CENTER);
		damagePerFlameLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel fieldOfViewLabel = new JLabel("Field of View", JLabel.CENTER);
		fieldOfViewLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel supplyRangeLabel = new JLabel("Supply Range", JLabel.CENTER);
		supplyRangeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel fireDangerRangeLabel = new JLabel("Fire Danger Range", JLabel.CENTER);
		fireDangerRangeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		JLabel tilesToDangerLabel = new JLabel("Tiles to Danger", JLabel.CENTER);
		tilesToDangerLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JSlider spreadRateSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, 30, 9);
		JSlider windInfluenceSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, 20, PERCENTAGE_INIT);
		JSlider vegInfluenceSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider waterEffectivenessSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider dangerPerFireSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider dangerPerVegSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider businessInfluenceSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider distanceInfluenceSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider endangeredInfluenceSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		JSlider squadDangerInfluenceSlider = new JSlider(JSlider.HORIZONTAL,
				PERCENTAGE_MIN, PERCENTAGE_MAX, PERCENTAGE_INIT);
		
		JSlider damagePerFlameSlider = new JSlider(JSlider.HORIZONTAL,
				0, 100, 10);
		JSlider fieldOfViewSlider = new JSlider(JSlider.HORIZONTAL,
				1, 20, 8);
		JSlider supplyRangeSlider = new JSlider(JSlider.HORIZONTAL,
				1, 20, 6);
		JSlider fireDangerRangeSlider = new JSlider(JSlider.HORIZONTAL,
				1, 10, 2);
		JSlider tilesToDangerSlider = new JSlider(JSlider.HORIZONTAL,
				1, 10, 4);
		
		spreadRateSlider.setName("spreadrate");
		windInfluenceSlider.setName("windinfluence");
		vegInfluenceSlider.setName("veginfluence");
		waterEffectivenessSlider.setName("watereffectiveness");
		dangerPerFireSlider.setName("dangerperfire");
		dangerPerVegSlider.setName("dangerperveg");
		businessInfluenceSlider.setName("businessinfluence");
		distanceInfluenceSlider.setName("distanceinfluence");
		endangeredInfluenceSlider.setName("endangeredinfluence");
		squadDangerInfluenceSlider.setName("squaddangerinfluence");
		damagePerFlameSlider.setName("damageperflame");
		fieldOfViewSlider.setName("fieldofview");
		supplyRangeSlider.setName("supplyrange");
		fireDangerRangeSlider.setName("firedangerrange");
		tilesToDangerSlider.setName("tilestodanger");
		
		spreadRateSlider.addChangeListener(this);
		windInfluenceSlider.addChangeListener(this);
		vegInfluenceSlider.addChangeListener(this);
		waterEffectivenessSlider.addChangeListener(this);
		dangerPerFireSlider.addChangeListener(this);
		dangerPerVegSlider.addChangeListener(this);
		businessInfluenceSlider.addChangeListener(this);
		distanceInfluenceSlider.addChangeListener(this);
		endangeredInfluenceSlider.addChangeListener(this);
		squadDangerInfluenceSlider.addChangeListener(this);
		
		damagePerFlameSlider.addChangeListener(this);
		fieldOfViewSlider.addChangeListener(this);
		supplyRangeSlider.addChangeListener(this);
		fireDangerRangeSlider.addChangeListener(this);
		tilesToDangerSlider.addChangeListener(this);
		
		config(spreadRateSlider);
		config(windInfluenceSlider);
		config(vegInfluenceSlider);
		config(waterEffectivenessSlider);
		config(dangerPerFireSlider);
		config(dangerPerVegSlider);
		config(businessInfluenceSlider);
		config(distanceInfluenceSlider);
		config(endangeredInfluenceSlider);
		config(squadDangerInfluenceSlider);
		config(damagePerFlameSlider);
		config(fieldOfViewSlider);
		config(supplyRangeSlider);
		config(fireDangerRangeSlider);
		config(tilesToDangerSlider);
	
		addTo(spreadRateSlider,spreadRateLabel);
		addTo(windInfluenceSlider,windInfluenceLabel);
		addTo(vegInfluenceSlider,vegInfluenceLabel);
		addTo(waterEffectivenessSlider,waterEffectivenessLabel);
		addTo(dangerPerFireSlider,dangerPerFireLabel);
		addTo(dangerPerVegSlider,dangerPerVegLabel);
		addTo(businessInfluenceSlider,businessInfluenceLabel);
		addTo(distanceInfluenceSlider,distanceInfluenceLabel);
		addTo(endangeredInfluenceSlider,endangeredInfluenceLabel);
		addTo(squadDangerInfluenceSlider,squadDangerInfluenceLabel);
		
		addTo(damagePerFlameSlider,damagePerFlameLabel);
		addTo(fieldOfViewSlider,fieldOfViewLabel);
		addTo(supplyRangeSlider,supplyRangeLabel);
		addTo(fireDangerRangeSlider,fireDangerRangeLabel);
		addTo(tilesToDangerSlider,tilesToDangerLabel);
		
		toggleGoalButton = new JButton("Enable Reactive team");
		toggleGoalButton.setVerticalTextPosition(AbstractButton.CENTER);
		toggleGoalButton.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
		toggleGoalButton.setActionCommand("enableReactive");
		toggleGoalButton.setToolTipText("Toggles the supplyWater goal of Firetruck "
                + "and requestSupply goal of Firefighter.");
		
		toggleGoalButton.addActionListener(this);
		
		add(toggleGoalButton);
		setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
	}
	
	public void actionPerformed(ActionEvent e) {
	    if ("enableReactive".equals(e.getActionCommand())) {
	    	toggleGoalButton.setEnabled(true);
	    	toggleGoalButton.setActionCommand("disableReactive");
	    	toggleGoalButton.setText("Disable Reactive team");
	    	
	    	for(int i=0;i<Util.trucks.size();i++) {
	    		Util.trucks.get(i).getAgent().dropGoal(Util.trucks.get(i).new SupplyWater());
	    	}
	    	for(int i=0;i<Util.fighters.size();i++) {
	    		Util.fighters.get(i).getAgent().dispatchTopLevelGoal(Util.fighters.get(i).new RequestSupply());
	    	}
	    	System.out.println("Dispatching Request Supply goal of Firefighter.");
	    } else {
	    	toggleGoalButton.setEnabled(true);
	    	toggleGoalButton.setActionCommand("enableReactive");
	    	toggleGoalButton.setText("Enable Reactive team");
	    	
	    	for(int i=0;i<Util.fighters.size();i++) {
	    		Util.fighters.get(i).getAgent().dropGoal(Util.fighters.get(i).new RequestSupply());
	    	}
	    	for(int i=0;i<Util.trucks.size();i++) {
	    		Util.trucks.get(i).getAgent().dispatchTopLevelGoal(Util.trucks.get(i).new SupplyWater());
	    	}
	    	System.out.println("Dispatching Supply Water goal of Firetruck.");
	    }
	}
	
	 synchronized public void stateChanged(ChangeEvent e) {
	        JSlider source = (JSlider)e.getSource();
	        if (!source.getValueIsAdjusting()) {
	            double val = source.getValue()/10.0;
	            if(source.getName().equalsIgnoreCase("spreadrate")) {
	            	FireProcess.spreadRate = val;
	            }
	            else if(source.getName().equalsIgnoreCase("windinfluence")) {
	            	FireProcess.windInfluence = val;
	            } else if(source.getName().equalsIgnoreCase("veginfluence")) {
	            	FireProcess.vegInfluence = val;
	            } else if(source.getName().equalsIgnoreCase("watereffectiveness")) {
	            	FirefighterBDI.waterEffectiveness = val;
	            } else if(source.getName().equalsIgnoreCase("dangerperfire")) {
	            	CommanderBDI.dangerPerFire = val;
	            } else if(source.getName().equalsIgnoreCase("dangerperveg")) {
	            	CommanderBDI.dangerPerVeg = val;
	            } else if(source.getName().equalsIgnoreCase("businessinfluence")) {
	            	CommanderBDI.businessInfluence = val;
	            } else if(source.getName().equalsIgnoreCase("distanceinfluence")) {
	            	CommanderBDI.distanceInfluence = val;
	            } else if(source.getName().equalsIgnoreCase("endangeredinfluence")) {
	            	CommanderBDI.endangeredInfluence = val;
	            } else if(source.getName().equalsIgnoreCase("squaddangerinfluence")) {
	            	CommanderBDI.squadDangerInfluence = val;
	            } else if(source.getName().equalsIgnoreCase("damageperflame")) {
	            	BaseAgentBDI.fireDangerRange = (int) (val*10.0);
	            } else if(source.getName().equalsIgnoreCase("fieldofview")) {
	            	BaseAgentBDI.fieldOfView = (int) (val*10.0);
	            } else if(source.getName().equalsIgnoreCase("supplyrange")) {
	            	FirefighterBDI.supplyRange = (int) (val*10.0);
	            } else if(source.getName().equalsIgnoreCase("firedangerrange")) {
	            	BaseAgentBDI.fireDangerRange = (int) (val*10.0);
	            } else if(source.getName().equalsIgnoreCase("tilestodanger")) {
	            	CommanderBDI.tilesToDanger = val*10.0;
	            }
	        }
	    }
	
	public void addTo(JSlider slider, JLabel label) {
		add(slider);
        add(label);
	}
	
	public void config(JSlider slider) {
		slider.setMajorTickSpacing(10);
		slider.setMinorTickSpacing(1);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
	}
	
	public static void createAndShowGUI() {
		JFrame frame = new JFrame("Firefighting GUI");
		FirefightingGui gui = new FirefightingGui();
		frame.add(gui);
		frame.setTitle("Firefighting GUI");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
}
